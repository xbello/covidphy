import json
import strformat

import jester

import ./forms
import ./models
import ./utils
import ./views
import helpers / csrf

router genbank:
  # Genbank ID
  get "":
    redirect "/gb/"
  get "/":
    let secret = newSecret()
    setToken secret
    getErrors
    resp genbankForm(secret, errors)
  post "/":
    if checkToken(request):
      var sequence: string
      let errs = isGenbankValid(@"genbankId", sequence)
      if len(errs) > 0:
        setCookie("formErrors", $(%*errs))
        redirect "/gb/"
      let id: string = alignMafft(sequence)
      discard updateStats(id, sequenceStats(sequence))
      redirect &"/align/result/{id}/"
    else:
      halt Http403
