import db_sqlite
import json
import os
import sequtils
import strformat
import strtabs
import strutils
import sugar
import tables
import times
import uri

import dotenv
import uuids

import data_clean
import helpers / [aligner, pathways, superspreaders, variantMaker]
import views / plotly_maps

let env = initDotEnv()
env.load()

const
  fields: Table[string, seq[string]] =
    {"result": @["uuid", "status", "created", "stats", "name", "group"],
     "variant": @["id", "alt", "ref", "ref_aa", "alt_aa", "position",
                  "position_aa", "orf_name", "kind", "severity", "description",
                  "frequency"],
     "gisaid": @["id", "haplogroup_id"],
     "country": @["id", "name", "latitude", "longitude", "continent"],
     "haplogroup": @["id", "name"],
     "concern_variant": @["id", "base", "aa"],
     "results_concernvariants": @["result_id", "concernvariant_id"],
     "superspreader_event": @["id", "start_date", "end_date", "first_date",
                              "last_date", "event_sequences",
                              "event_total_sequences", "n_sequences",
                              "total_sequences", "near_sequences", "image"],
     "results_seevents": @["result_id", "seevent_id"]
     }.toTable

type
  Kind* = enum
    kUndefined = (0, "Undefined"),
    kMutation = (1, "Mutation"),
    kInsertion = (2, "Insertion"),
    kDeletion = (3, "Deletion")

  Severity* = enum
    sLow = (0, "Low"),
    sMedium = (1, "Medium"),
    sHigh = (2, "High")

  Status* = enum
    stError = (0, "Error"),
    stPending = (1, "Pending"),
    stRunning = (2, "Running"),
    stDone = (3, "Done"),

  Variant* = tuple
    id, alt, refer, ref_aa, alt_aa, position, position_aa: string
    orf_name, kind, severity, description, frequency: string

  HaplogroupSum* = tuple
    id, country: string
    count, total: int

template getConn* =
  let db {.inject.}: DbConn = open(os.getEnv("db"), "", "", "")
  defer: db.close

proc loadFixture*(db: DbConn, fixtureFile: string) =
  ## A fixture is a SQL file created with the .dump SQLite command
  ## These fixtures are created within the DB with:
  ##   .output "fixture_name.sql"
  ##   .dump "table_name"
  ##
  let fix: File = open(fixtureFile)
  defer: fix.close

  var sqlCommand: string
  for line in fix.lines:
    if not line.startsWith("--"):
      sqlCommand.add(line)
      if line.endsWith(";"):
        db.exec(sql(sqlCommand))
        sqlCommand = ""

proc rowAsData(columns: seq[string], row: Row): StringTableRef =
  ## Turn a single Row into StringTableRef assigned to columns
  ##
  result = newStringTable()
  for (col, value) in zip(columns, row):
    result[col] = value

proc rowsAsData(columns: seq[string], rows: seq[Row]): seq[StringTableRef] =
  ## Turn Rows into StringTableRefs
  ##
  for row in rows:
    result.add rowAsData(columns, row)

proc get*(db: DbConn, tableName: string, pk: string): StringTableRef =
  ## Get a single row using the pk
  ##
  let pkField: string = db.getValue(
    sql"SELECT name FROM pragma_table_info(?) WHERE pk = 1;", tableName)
  let quotedFields = join(mapIt(fields[tableName], &"\"{it}\""), ", ")

  let values = db.getRow(
    sql(&"""SELECT {quotedFields} FROM ? WHERE {pkField} = ?"""),
    tableName, pk)

  rowAsData(fields[tableName], values)

proc variantId*(db: DbConn, r, a: char, pos: int): int64 =
  ## Check if a variant exist, returning the Id field it it does, 0 otherwise
  ##
  let variantId = db.getValue(sql"""SELECT id FROM variant
                              WHERE ref=? AND alt=? AND position = ?""",
                              r, a, pos)

  if variantId == "": result = 0 else: result = parseInt(variantId)

proc insertNewVariant*(db: DbConn, r, a: char, pos: int): int64 =
  ## Insert a new variant if it doesn't exits, returning the Id field
  result = db.variantId(r, a, pos)

  if result == 0:
    # The variant doesn't exist. Insert the values, return the id.
    let mut: Mutation = newMutation(pos, a)
    if mut.pos > 0:
      # Mutation IN of ORF
      result = db.tryInsertID(sql"""INSERT OR IGNORE INTO variant (ref, alt,
        position, ref_aa, alt_aa, position_aa, orf_name, kind, severity,
        description)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
        r, a, pos, mut.refer_aa, mut.alt_aa, mut.pos, mut.orf, mut.kind,
        mut.severity, mut.desc)
    else:
      # Mutation OUT of ORF
      # Without ref_aa/alt_aa, kind, severity or description,
      # while having a NOT NULL constraint
      result = db.tryInsertID(sql"""INSERT OR IGNORE INTO variant (ref, alt,
        position, kind, severity, description)
        VALUES (?, ?, ?, ?, ?, ?)""",
        r, a, pos, "", "", mut.kind, sLow, "Out of ORF")

proc updateVariant*(db: DbConn, pk: int64,
                    updateFields: varargs[tuple[key, val: string]]): int64 =
  ## Update a variant with the data in "fields".
  ##
  ## If a field.key doesn't exist, it won't be included in the update
  ##
  var sqlUpdateFields: seq[string]
  var sqlUpdateValues: seq[string]
  for field in updateFields:
    if field.key in fields["variant"]:
      sqlUpdateFields.add(field.key & " = ?")
      sqlUpdateValues.add field.val

  sqlUpdateValues.add($pk)

  let sqlStatement = &"""UPDATE OR IGNORE variant
    SET {join(sqlUpdateFields, ",")}
    WHERE id = ?"""

  db.execAffectedRows(sql(sqlStatement), sqlUpdateValues)

proc updateStats*(pk: string, stats: JsonNode): int64 =
  getConn
  let sqlStatement = """UPDATE OR IGNORE result
    SET stats = ?
    WHERE uuid = ?"""

  db.execAffectedRows(sql(sqlStatement), stats, pk)

proc joinValues(uuid: string, variants: seq[int64]): string =
  for variant in variants:
    if variant > 0: # -1 means that the insertion failed, the ID is invalid
      result.add &"('{uuid}','{variant}'),"
  result.strip(chars={','})

proc addResultName*(uuid: string, groupName: string): bool =
  ## Insert Group and Name into a Result
  ##
  getConn
  let sqlStatement = """UPDATE OR IGNORE result
    SET "name" = ?, "group" = ?
    WHERE "uuid" = ?"""

  let (name, group) = cleanId(groupName)

  return db.execAffectedRows(sql(sqlStatement), name, group, uuid) == 1

proc addVariantsToResult*(db: DbConn, uuid: string, variants: seq[int64]) =
  if len(variants) > 0:
    db.exec(sql"""BEGIN TRANSACTION""")
    db.exec(sql &"""INSERT OR ROLLBACK INTO results_variants
            (result_id, variant_id) VALUES {joinValues(uuid, variants)}""")
    db.exec(sql"""COMMIT TRANSACTION""")

proc createResult*(db: DbConn): UUID =
  result = genUUID()

  db.exec(sql"""
    INSERT INTO "result" (uuid, status, created) VALUES (?, ?, ?)""",
    result, stPending, now())

proc sequenceStats*(sequence: string): JsonNode =
  ## Makes some stats over original sequence, targeting the DB as a Json
  ##
  ## - Doesn't take gaps (-) into account
  ## - Counts "bases" different from ACGT
  ## - Counts "bases" ACGT
  ##
  result = %* {}
  var frequencies = initCountTable[char]()
  let s = sequence.replace("-", "")

  for line in s.splitLines:
    if line.startsWith('>'):
      continue
    for c in line:
      frequencies.inc(c)

  var length: int = 0
  for base, c in frequencies.pairs:
    if base notin {'A', 'C', 'G', 'T'}:
      result[$base] = newJint c
    else:
      length.inc c

  result["length"] = newJInt length

proc getResultStats*(result_uuid: string): JsonNode =
  getConn
  let stats: string = db.getValue(sql"SELECT stats FROM result WHERE uuid=?",
                                  result_uuid)
  if not stats.isEmptyOrWhitespace:
    return parseJson(stats)
  return %* {}

proc getAllConcernVariants*: seq[StringTableRef] =
  getConn
  let table = "concern_variant"
  let quotedFields = join(mapIt(fields[table], &"\"{it}\""), ", ")

  let query: string = &"""SELECT {quotedFields} FROM {table}"""

  let dataRows = db.getAllRows(sql(query))

  rowsAsData(fields[table], dataRows)

proc insertResultConcernVariants*(result_uuid: string,
                                  concern_variants: seq[string]): bool =
  ## Insert the ConcernVariants IDs as related to the Result
  # Shortcut for empty concern_variants
  if len(concern_variants) == 0: return true

  getConn
  let table = "results_concernvariants"
  let quotedFields = join(mapIt(fields[table], &"\"{it}\""), ", ")
  var qHolders: seq[string]
  var values: seq[string]
  for cv in concern_variants:
    qHolders.add "(" & join(repeat("?", len(fields[table])), ", ") & ")"
    values.add result_uuid
    values.add cv

  let query: string = &"""INSERT OR IGNORE INTO {table}
    ({quotedFields})
    VALUES {join(qHolders, ", ")}"""

  db.tryExec(sql(query), values)

proc getAlignConcernVariants*(align: seq[string]): seq[string] =
  ## Returns the IDs of the Concern Variants found in the Alignment
  ##
  getConn
  let allCVs = getAllConcernVariants()  # Hits the DB
  let seqDiffs = findDiffs(align, mapIt(allCVs, it["base"]))

  result = collect(newSeq):
    for concernVariant in allCVs:
      if concernVariant["base"] in seqDiffs:
        concernVariant["id"]

proc getAllSuperspreadEvents*(countries: bool = false): seq[StringTableRef] =
  getConn
  let table = "superspreader_event"
  let quotedFields = join(mapIt(fields[table], &"\"{table}\".\"{it}\""), ", ")

  let query: string =
    if countries:
      &"""SELECT {quotedFields}, country.name, haplogroup.name FROM {table}
      JOIN country ON country.id = {table}.country_id
      JOIN haplogroup ON haplogroup.id == {table}.haplogroup_id
      ORDER BY country.name, start_date"""
    else:
      &"""SELECT {quotedFields} FROM {table}"""

  let dataRows = db.getAllRows(sql(query))

  let fields =
    if countries:
      fields[table] & "country.name" & "haplogroup.name"
    else:
      fields[table]

  rowsAsData(fields, dataRows)

proc getSuperspreadWithCountries*(ids: seq[string]): seq[StringTableRef] =
  ## Get superspreader events including Country name.
  ##
  getConn
  let table = "superspreader_event"
  let quotedFields = join(mapIt(fields[table], &"\"{table}\".\"{it}\""), ", ")
  let holders = join(repeat('?', len(ids)), ",")
  var query: string = &"""
    SELECT {quotedFields}, country.name, haplogroup.name FROM {table}
    JOIN country ON country.id = {table}.country_id
    JOIN haplogroup ON haplogroup.id == {table}.haplogroup_id
    WHERE {table}.id IN ({holders})"""

  let dataRows = db.getAllRows(sql(query), ids)

  let fields = fields[table] & "country.name" & "haplogroup.name"
  rowsAsData(fields, dataRows)

proc getSuperspreaderEvents*(align: seq[string]): seq[string] =
  ## Returns all the potential superspreader events for the alignment
  ##
  ## Values returned are IDs *from the phylogeny*, that should match the ID
  ##  in the DB.
  let variants = findDiffs(align, superspreadersPhy.variants)
  getEvents(superspreadersPhy, variants)

proc insertResultSuperspreads*(result_uuid: string,
                               seevents: seq[string]): bool =
  ## Insert the Superspreader Event IDs as related to the Result
  if len(seevents) == 0: return true

  getConn
  let table = "results_seevents"
  let quotedFields = join(mapIt(fields[table], &"\"{it}\""), ", ")
  var qHolders: seq[string]
  var values: seq[string]
  for seevent in seevents:
    qHolders.add "(" & join(repeat("?", len(fields[table])), ", ") & ")"
    values.add result_uuid
    values.add seevent

  let query: string = &"""INSERT OR IGNORE INTO {table}
    ({quotedFields})
    VALUES {join(qHolders, ", ")}"""

  db.tryExec(sql(query), values)

proc alignMafft*(seq: string, saveFile: bool = false): string =
  getConn
  let uuid: UUID = createResult(db)
  result = $uuid

  var targetFile: string
  if saveFile:
    let alignPath = getCurrentDir() / "public" / os.getEnv("alignDir")
    createDir(alignPath)
    targetFile = alignPath / $uuid & ".fasta"

  let alignment: seq[string] = diffString(seq, saveAs=targetFile)
  # Search variants in the alignment
  let variants: seq[string] = diffs(alignment)
  # Search and insert concern variants found in the alignment
  let concernVariantIds: seq[string] = getAlignConcernVariants(alignment)
  discard insertResultConcernVariants(result, concernVariantIds)
  # Get the superspreaders events Ids from the alignment
  let seeventsIds: seq[string] = getSuperspreaderEvents(alignment)
  discard insertResultSuperspreads(result, seeventsIds)

  var results_variants: seq[int64]

  db.exec(sql"BEGIN TRANSACTION")
  for variant in variants:
    var refer: char = variant[0]
    var alt: char = variant[^1]
    var pos: int = variant[1 .. ^2].parseInt

    results_variants.add db.insertNewVariant(refer, alt, pos)
  db.exec(sql"COMMIT TRANSACTION")

  db.addVariantsToResult(result, results_variants)

  db.exec(sql"""
    UPDATE OR IGNORE result SET status = ? WHERE uuid = ?""", stDone, uuid)

proc resultExists*(result_uuid: string): bool =
  ## Check if the result exists
  getConn
  let uuid = db.getValue(sql"""
    SELECT uuid FROM result WHERE uuid=?""", result_uuid)

  result = if uuid == "": false else: true

proc setVariants*(vs: seq[Variant]): seq[string] =
  # Transform a seq[Variant] from the DB into a HashSet of stringed variants
  for v in vs:
    result.add &"{v.refer}{v.position}{v.alt}"

proc getConcernVariant*(base: string): StringTableRef =
  getConn
  let table = "concern_variant"
  let quotedFields = join(mapIt(fields[table], &"\"{it}\""), ", ")
  let query: string = &"""
    SELECT {quotedFields} FROM {table} WHERE "base" = ?"""

  let concernVariant = db.getRow(sql(query), base)

  rowAsData(fields[table], concernVariant)

proc getResultConcernVariants*(result_uuid: string): seq[StringTableRef] =
  getConn
  let table = "concern_variant"
  let quotedFields = join(mapIt(fields[table], &"\"{it}\""), ", ")
  let query: string = &"""
    SELECT {quotedFields} FROM {table}
    JOIN results_concernvariants
    ON results_concernvariants.concernvariant_id == concern_variant.id
    WHERE results_concernvariants.result_id = ?"""

  let dataRows = db.getAllRows(sql(query), result_uuid)

  rowsAsData(fields[table], dataRows)

proc getResultVariants*(result_uuid: string): seq[Variant] =
  getConn
  let dbData = db.getAllRows(sql"""SELECT variant.id, alt, ref, ref_aa, alt_aa,
    position, position_aa, orf_name, kind, severity, description, frequency
    FROM variant
    JOIN results_variants
    ON results_variants.variant_id = variant.id
    WHERE results_variants.result_id = ?
    ORDER BY position
    LIMIT 500""",
    result_uuid)
  for v in dbData:
    result.add (v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8], v[9],
                v[10], v[11])

proc filterResultGroup*(uuid: string): seq[StringTableRef] =
  ## Return the Results included in the same group as uuid
  ##
  if uuid.isEmptyOrWhitespace: return

  getConn
  let table: string = "result"
  let quotedFields = join(mapIt(fields[table], &"\"{it}\""), ", ")
  let query: string = &"""
    SELECT {quotedFields} FROM {table}
    WHERE "group" = (SELECT "group" FROM {table} WHERE "uuid" = ?)"""

  let dataRows = db.getAllRows(sql(query), uuid)

  rowsAsData(fields[table], dataRows)

proc getResultSuperspreaderEvents*(result_uuid: string): seq[StringTableRef] =
  getConn
  let query: string = &"""
    SELECT "id" FROM superspreader_event
    JOIN results_seevents
    ON results_seevents.seevent_id == superspreader_event.id
    WHERE results_seevents.result_id = ?"""

  let dataRows = collect(newSeq):
    for id in db.getAllRows(sql(query), result_uuid):
      id[0]

  if len(dataRows) == 0: return

  getSuperspreadWithCountries(dataRows)

proc classify*(variants: seq[Variant]): seq[Haplogroup] =
  classifyInHaplogroups(phylogeny, setVariants(variants))

proc selectHaplogroup*(haplogroup, continent: string = ""): seq[Row] =
  ## Select a given haplogroup for all countries in continent.
  ## If continent is omited, all countries for all continents are included.
  getConn
  var continentClause: string = "cB.name != ?"
  if continent != "":
    continentClause = "cB.name = ?"

  var query: string = &"""
    SELECT cA.name, cA.latitude, cA.longitude, countries_haplogroups.count,
     (SELECT SUM(countries_haplogroups.count)
      FROM countries_haplogroups
      WHERE id_country = cA.id),
    cB.name
    FROM country cA
    JOIN countries_haplogroups ON id_country = cA.id
    JOIN haplogroup ON haplogroup.id = id_haplogroup
    JOIN country cB ON cB.id = cA.continent
    WHERE haplogroup.name = ?
    AND {continentClause}
    ORDER BY cA.name"""

  db.getAllRows(sql(query), haplogroup, decodeUrl(continent))

proc selectWorldHaplogroup*(haplogroup: string): seq[Row] =
  ## Same as selectHaplogroup, but with the special case of grouping per
  ##  continent.
  getConn
  db.getAllRows(sql"""
    SELECT cB.name, cB.latitude, cB.longitude,
    SUM(countries_haplogroups.count),
     (SELECT SUM(countries_haplogroups.count)
     FROM countries_haplogroups
     WHERE id_country = cA.continent),
    cB.continent
    FROM country cA
    JOIN countries_haplogroups ON id_country = cA.id
    JOIN haplogroup ON haplogroup.id = id_haplogroup
    JOIN country cB ON cB.id = cA.continent
    WHERE haplogroup.name = ?
    GROUP BY cB.name""",
  haplogroup)

proc newPoint(row: Row): Point =
  ## Create a Point object from given Row from the DB
  ##
  Point(name: row[0],
        # A point is its own continent if continent is NULL (empty)
        continent: if row[5] != "": row[5] else: row[0],
        lat: row[1].parseFloat,
        lon: row[2].parseFloat,
        n: row[3].parseInt,
        total: row[4].parseInt)

proc selectHaplogroupLocCount*(haplogroup: string): seq[Point] =
  ## Select haplogroups for all the continents.
  for d in selectWorldHaplogroup(haplogroup):
    result.add newPoint(d)

proc selectHaplogroupLocCount*(haplogroup, area: string): seq[Point] =
  ## Same as selectHaplogroupLocCount but restricting to a given area.
  ##
  for d in selectHaplogroup(haplogroup, area):
    result.add newPoint(d)

proc selectHaplogroupByName*(haplogroup: string): seq[HaplogroupSum] =
  for h in selectHaplogroup(haplogroup):
    result.add (id: haplogroup, country: h[0],
                count: h[3].parseInt, total: h[4].parseInt)

proc selectCountGisaid*: int =
  ## Return the total number of Gisaid sequences pre-classified
  ##
  getConn
  db.getValue(sql"""SELECT count(id) FROM gisaid""").parseInt

proc selectMinMaxGisaid*: tuple[minV, maxV: int] =
  ## Return a Tuple with minimum and maximum values for GisaIDs.
  ##
  getConn
  var minMax: Row = db.getRow(sql"""SELECT min(id), max(id) FROM gisaid;""")

  (minV: minMax[0].parseInt, maxV: minMax[1].parseInt)

proc selectHaplogroupByGisaid*(gisaidID: string): string =
  ## Return the haplogroup ID given a gisaid ID.
  getConn
  db.getValue(sql"""
    SELECT haplogroup.name FROM haplogroup
    JOIN gisaid ON haplogroup_id = haplogroup.id
    WHERE gisaid.id = ?""",
    gisaidID)
