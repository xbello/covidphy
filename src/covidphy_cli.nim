import json
import os
import parseopt
import sequtils
import strformat
import strutils
import bio / fasta
import helpers / [aligner, pathways]


const bases = {'A', 'C', 'G', 'T'}
const version = strip(staticRead("VERSION"))

proc cleanSequence*(s: string): string =
  ## Return a string with only bases
  for b in s:
    if b.toUpperAscii in bases:
      result.add b

proc getSequence*(fasta: string): SequenceRecord =
  ## Return a SequenceRecord from the stdin input
  ##
  var chain, name: string
  for line in fasta.splitLines:
    if line.startsWith(">"):
      if name == "":
        name = line[1 .. ^1]
      else: break # The stdin includes multiple sequences, cut the reading.
    else:
      chain.add cleanSequence(line)

  SequenceRecord(name: name, record: newDna(chain))

proc main*(sr: SequenceRecord): JsonNode =
  ## Aligns a SequenceRecord against the Reference, to find the differences
  ##  among them and classify it in two haplogroups: one without reversion,
  ##  one allowing a reversion.
  ##
  ##  A Json is returned, with the following schema:
  ##
  ## {"haplogroups": [
  ##   {"name":"A",
  ##    "pathway":[]}
  ##    ],
  ##  "variants": ["T2C"...],
  ##  "length":29757}
  ##
  result = %* {
    "haplogroups": [],
    "variants": [],
    "length": 0
  }

  result["length"] = %* count(sr.record.chain, bases)

  let variants = diffs(diffString(sr.record.chain))

  result["variants"] = %* toSeq(variants)

  for haplogroup in classify(phylogeny, variants):
    result["haplogroups"].add(%* {
      "name": haplogroup,
      "pathway": getPathway(phylogeny, haplogroup)
    })

proc parseCmd(params: seq[string]): SequenceRecord =
  var p = initOptParser(join(params))

  let doc = &"  CovidPhy - {version}\n" &
  """========
  Classify a fasta sequence into haplogroups, returning a Json representation.

  Usage:
    covidphy_cli --fasta:path/to/file.fas
    covidphy_cli --f:path/to/file.fas
    cat path/to/file.fas | covidphy_cli

  Options:
    -h --help    Show this message.
    -v --version Show the version.
    -f --fasta   The path to the fasta file. Only the first sequence of the
                 file is going to be used.

  Returns:
    A Json is returned, with the following schema:

   {"haplogroups": [
     {"name":"A",
      "pathway":[]}
    ],
    "variants": ["T2C"...],
    "length":29757}

  """

  while true:
    p.next()
    case p.kind
    of cmdEnd: break
    of cmdShortOption, cmdLongOption:
      if p.key in ["h", "help"]:
        quit doc
      if p.key in ["v", "version"]:
        quit version
      else:
        if p.key in ["f", "fasta"]:
          if os.fileExists(p.val):
            result = load(p.val)[0]
            result.record.chain = cleanSequence(result.record.chain)
            return
          else:
            quit &"{doc}\nError: File '{p.val}' not found."
    else: discard

  # No options? Read from stdin then.
  getSequence(readAll(stdin))

when isMainModule:
  let fastaInput = parseCmd(commandLineParams())
  echo main(fastaInput)
