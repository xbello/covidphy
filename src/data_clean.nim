import sequtils
import strutils

proc cleanGisaidId*(gisaidId: string): string =
  result = normalize(gisaidId)
  if result.startsWith("epiisl"):
    result = result[6 .. ^1]

proc cleanId*(idString: string): tuple[name, group: string] =
  ## Remove unwanted chars from the idString and split the data into a tuple
  ##
  ## If no ":" found, idString is assigned to "name"
  ## If ":" found, first part is "group", last part is "name"
  let data = join(filterIt(idString, it in Letters + Digits + {':'}))
    .split(":", maxSplit=1)

  case len(data)
  of 1:
    result.name = data[0]
  of 2:
    result.name = data[1]
    result.group = data[0]
  else:
    discard

proc cleanSequence*(sequence: string): string =
  var count: int
  for line in splitLines(sequence):
    if line.startsWith(">"): continue
    for c in line:
      if c.toUpperAscii in {'A', 'C', 'G', 'T'}:
        result.add c
        inc count
        if count == 1 shl 15:
          break
