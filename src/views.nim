import views / [about, alignForm, gbForm, gisaidForm, haplogroup, resultDetail,
                resultJson, superspreader]
import views / [home, errPages]

export about
export alignForm
export errPages
export gbForm
export gisaidForm
export haplogroup
export home
export resultDetail, resultJson
export superspreader
