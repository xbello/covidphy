import json
import logging
import os
import strformat
import strutils
import times

import dotenv
import jester

import ./forms
import ./logs
import ./models
import ./urls
import ./utils
import ./views
import helpers / csrf

let env = initDotEnv()
env.load()

var mLogger = newMailLogger()
var cLogger = newConsoleLogger(fmtStr=verboseFmtStr, levelThreshold=lvlWarn)
addHandler(mLogger)
addHandler(cLogger)

proc `$`(p: Port): string {.borrow.}

settings:
  bindAddr = os.getEnv("bindAddr")
  port = Port(os.getEnv("port").parseInt)

routes:
  warn(&"Serving at http://{settings.bindAddr}:{settings.port}")
  get "":
    redirect "/"
  get "/":
    resp homeView()

  extend result, "/align"
  extend genbank, "/gb"
  extend gisaid, "/gisaid"
  extend phylogeny, "/phylogeny"
  extend superspreader, "/superspread"

  # About
  get "/about":
    redirect "/about/"
  get "/about/":
    resp aboutTemplate()
  error Http404:
    warn(&"404. {now()}. IP: {request.ip}. URL: {request.path}. PARAMS: {request.params}.")
    resp Http404, custom404()
  error Exception:
    let msg = &"Path: {request.path}\n" &
              &"Params: {request.params}\n" &
              &"Request IP: {request.ip}\n" &
              &"Full info: {exception.msg}"
    error(Http500, msg)
    resp Http500, custom500()
