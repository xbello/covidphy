import jester

import ./views

router phylogeny:
  # Pathways views
  get "":
    redirect "/phylogeny/"
  get "/":
    resp haplogroupList()
  get "/@name":
    redirect "/phylogeny/" & @"name" & "/"
  get "/@name/":
    resp haplogroupDetail(@"name")
  get "/@name/@area":
    redirect "/phylogeny/" & @"name" & "/" & @"area" & "/"
  get "/@name/@area/":
    resp haplogroupDetail(@"name", @"area")
