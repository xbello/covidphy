##
## Updates the frequencies from the latest SQL fixture into the running DB.
##

import db_sqlite
import os

import ../models


proc main(fixture: string) =
  let dbTmp = open(":memory:", "", "", "")
  defer: dbTmp.close

  dbTmp.loadFixture(fixture)

  getConn
  db.exec(sql"BEGIN TRANSACTION")
  for variant in db.rows(sql"""SELECT alt, ref, position FROM variant"""):
    let frequency = dbTmp.getValue(
      sql"""SELECT frequency FROM variant
     WHERE alt=? AND ref=? AND position=?""",
     variant)
    db.exec(sql"""UPDATE OR IGNORE variant SET frequency = ?
             WHERE alt=? AND ref=? AND position=?""",
             frequency, variant[0], variant[1], variant[2])
  db.exec(sql"END TRANSACTION")

when isMainModule:
  doAssert(len(commandLineParams()) > 0)
  main(commandLineParams()[0])
