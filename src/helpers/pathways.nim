## Loads a Pathways file (pathways.marshal) into an object
## {"variants": ["A187G", ...],
##  "haplogroups": {"A", @[0, 0, 1234356...]}]}
##
## The sequence of integers in haplogroups are the presence or not of the
##  variants in the sequence. E.g.
##
##  variants = ["A1T", "A2G", "C3T", "G4A"]
##  haplogroup "A" has the signature ["A1T"]
##  haplogroup "B" has the signature ["C3T", "G4A"]
##
##  A is encoded as 1000 -> 8
##  B is encoded as 0011 -> 3
##
## Then it finds the best(s) Haplogroups to match a given set of variants.
##
import bitops
import math
import marshal
import os
import sequtils
import sets
import strformat
import strutils
import sugar
import tables

type
  Phylogeny* = object
    variants*: OrderedSet[string]
    signature*: Table[string, seq[int64]]

  Haplogroup* = object of RootObj
    name*: string
    pathway*: seq[string]

const word: int = 64
const phylogeny*: Phylogeny =
  staticRead(currentSourcePath.parentDir() / "Phylogeny.marshal").to[:Phylogeny]

proc getPathway*(phylogeny: Phylogeny, key: string): seq[string] =
  ## Get the pathway from a given haplogroup in Phylogeny
  ##
  ## I.e. undo the operation of buildSignature below
  ##
  if not phylogeny.signature.hasKey(key):
    return
  let variants = toSeq(phylogeny.variants)

  collect(newSeqOfCap(len(phylogeny.signature[key]))):
    for i, bit in join(mapIt(phylogeny.signature[key], it.toBin(word))):
      if unlikely(bit == '1'):
        variants[i]

proc getVariants*(refSeq, querySeq: string): seq[string] =
  ## Get the variants in query sequence compared to reference.
  ## It only looks after the positions in Phylogeny.variants
  ##
  var refPos: int
  let targetPos: seq[int] = mapIt(phylogeny.variants, parseInt(it[1 .. ^2]))

  for i, nucleotide in querySeq:
    if refSeq[i] != '-':
      refPos.inc
    if (refPos in targetPos):
      if nucleotide != refSeq[i] and nucleotide notin {'-', 'n', 'N'}:
        result.add &"{refSeq[i].toUpperAscii}{refPos}{nucleotide.toUpperAscii}"

proc diffs*(sig1, sig2: seq[int64]): int =
  for (v1, v2) in zip(sig1, sig2):
    # Turns out Nim already has an optimization for this!
    result.inc countSetBits(v1 xor v2)

proc buildSignature*(phylogeny: Phylogeny, vars: seq[string]): seq[int64] =
  ## Builds a decimal signature from a list of variants.
  ## Why?
  ##  It allows for further (and fastests) comparisons down the stream than
  ##   comparing strings-to-strings.
  ## How?
  ##  If a variant is found in common with one pathway, increments the decimal
  ##   in one. Then shifts the number (binary) to the left. Visually:
  ##
  ##   0 -> Exists -> 1 -> 10 ... -> 01110010
  ##     -> Don't  -> 0 -> 00 ... -> 10010100
  ##
  # Creates as many buckets of 64 bits as needed for phylogeny.variants
  const word: int = 64
  result = repeat(0'i64, int(ceil(len(phylogeny.variants) / word)))

  if len(vars) == 0: return

  let setVars = toHashSet(vars)

  for i, v in phylogeny.variants:
    if unlikely(v in setVars):
      # (i div word) finds the bucket index in result
      # ((word - 1) - (i mod word)) calculates the reversed bit index
      # For compatibility reasons, we have to reverse the bit setting.
      # TODO: Maybe change everything? Low priority.
      result[i div word].setBit((word - 1) - (i mod word))

proc classify*(phylogeny: Phylogeny, vars: seq[string], maxDiffs: int = 1000):
  seq[string] =
  ## Return a seq of haplogroup names with the best(s), if diffs are below
  ## maxDiffs threshold
  let signature = buildSignature(phylogeny, vars)

  var currentDiffs, bestDiffs: int
  bestDiffs = len(phylogeny.variants)

  for name, pathway in phylogeny.signature:
    currentDiffs = diffs(signature, pathway)

    if maxDiffs < currentDiffs:
      continue

    if currentDiffs < bestDiffs:
      bestDiffs = currentDiffs
      result = @[name]
    elif currentDiffs == bestDiffs:
      result.add name

proc classifyInHaplogroups*(phylogeny: Phylogeny, vars: seq[string]):
  seq[Haplogroup] =
  ## Returns the Haplogroup(s) object(s) given a sequence of variants
  ##
  for haplogroup in classify(phylogeny, vars):
    result.add(Haplogroup(name: haplogroup,
                          pathway: getPathway(phylogeny, haplogroup)))

when isMainModule:
  let varsA = @["C241T", "C1059T", "C3037T", "C14408T", "A23403G", "G25563T"]
  let varsB = @["C241T", "C3037T", "C14408T", "C18877T", "A23403G", "G25563T"]

  let undef = @["C241T", "C1059T", "C3037T", "C14408T", "C18877T", "A23403G", "G25563T"]

  echo classifyInHaplogroups(phylogeny, varsA)
  echo classifyInHaplogroups(phylogeny, undef)
