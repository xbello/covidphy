import dotenv
import os

import bio / entrez

let env = initDotEnv()
env.load()


proc getGenbankSeq*(gbId: string): string =
  ## Retrieves a Genbank sequence through Entrez
  ##
  let etrz = newEntrez(apiKey=os.getEnv("entrezKey"))

  let sr = etrz.fetchId(db="nuccore", id=gbId)

  sr.record.chain
