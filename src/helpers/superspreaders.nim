## Loads a Superspreaders file (superspreaders.marshal) into an object
## {"variants": ["A187G", ...],
##  "haplogroups": {"1", @[0, 0, 1234356...]}]}
##
## See sibling pathways.nim for more info
##
import marshal
import tables

import pathways

const superspreadersPhy*: Phylogeny =
  staticRead("Superspreaders.marshal").to[:Phylogeny]

proc getEventSignature*(phylogeny: Phylogeny, eventId: string): seq[int64] =
  ## Return the seq of signatures for the eventId.
  ##
  phylogeny.signature.getOrDefault(eventId)

proc getEventPathway*(phylogeny: Phylogeny, eventId: string): seq[string] =
  ## Return the pathway (the variants) of a superspreader Event.
  getPathway(phylogeny, eventId)

proc getEvents*(phylogeny: Phylogeny, vars: seq[string]): seq[string] =
  ## Return a seq with the superspreader events Ids of a sequence of variants
  ##
  ## If the difference is greater than 1, ignore it
  ##
  classify(phylogeny, vars, maxDiffs=1)

proc getEventsDetails*(phylogeny: Phylogeny, vars: seq[string]):
  seq[Haplogroup] =
  ## Return details of the Superspreading Event
  ##
  for event in classify(phylogeny, vars, maxDiffs=1):
    result.add(Haplogroup(name: event,
                          pathway: getEventPathway(phylogeny, event)))
