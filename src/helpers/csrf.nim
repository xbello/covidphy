# Stolen from https://github.com/planety/prologue
#
import cookies
import httpcore
import random
import sequtils
import strtabs
import strutils
import tables
from jester import ResponseData, setCookie
import jester/request

const
  DefaultTokenName = "CSRFToken"
  DefaultSecretSize* = 32
  DefaultTokenSize* = 64
  pool = IdentChars.toSeq

randomize()

proc maskToken*(secret: string): string =
  # Masks the Secret into a token
  var mask, cipher: string
  for i in 0 ..< DefaultSecretSize:
    mask.add sample(pool)
    cipher.add pool[(pool.find(secret[i]) + pool.find(mask[i])) mod len(pool)]

  mask & cipher

proc unmaskToken*(token: string): string =
  # Recover the Secret from the token
  var idx: int
  for i in 0 ..< DefaultSecretSize:
    idx = pool.find(token[DefaultSecretSize + i]) - pool.find(token[i])
    if idx < 0: idx = pool.len + idx
    result.add pool[idx]

proc newSecret*(): string =
  # Creates a new Secret string to use as Csrf Token.
  for i in 0 ..< DefaultSecretSize:
    result.add sample(pool)

template setToken*(secret: string): untyped =
  # Set the token into a Cookie
  jester.setCookie(DefaultTokenName, maskToken(secret), path="/")

proc checkToken*(req: Request, tokenName = DefaultTokenName): bool {.inline.} =
  # Check that the Tokens in Request for cookie and form matches
  let form = req.params().getOrDefault(tokenName)
  let cookie = parseCookies(req.headers.getOrDefault("cookie")).getOrDefault(tokenName)

  unmaskToken(form) == unmaskToken(cookie)
