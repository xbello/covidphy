import os
import sequtils
import sets
import strformat
import strscans
import strutils
import tables

import bio / fasta

proc getLib: string =
  let thisPath = currentSourcePath.parentDir()
  if fileExists(thisPath / "libdisttbfast.so"):
    result = thisPath / "libdisttbfast.so"

proc mafftAlign(rawSeqs: cstring): cstring
  {.importc: "disttbfast", dynlib: getLib().}

const reference = staticRead("resources/Reference.fasta").splitLines()

proc preAlign*(query: string): string =
  ## Creates the string required by mafftAligner to work
  ##
  var pair: seq[string]

  pair.add ">" & reference[0][1 .. ^1]
  pair.add join(reference[1 .. ^1])
  pair.add ">Query"
  pair.add toLowerAscii(query)

  join(pair, "\n")

proc align*(sequences: string): seq[string] =
  ## MAFFT requires a string with all the sequences joined into a string:
  ##   >name\nCATCAG\n>name2\nCACATGCATGCA
  ##
  ## This proc receives that string, passes it to MAFFT library, and returns
  ##  the (two, in our pipeline) seqs aligned.
  ##
  let alignment = split($mafftAlign(sequences), "\n")

  result = @[alignment[1], alignment[3]]

proc diffs*(align: seq[string]): seq[string] =
  var refPos: int

  let refSeq = align[0]
  var mutation: string
  for i, nucleotide in align[1].pairs:
    if refSeq[i] != '-':
      refPos.inc
    if nucleotide != refSeq[i] and nucleotide notin {'-', 'n', 'N'}:
      mutation = &"{refSeq[i].toUpperAscii}{refPos}{nucleotide.toUpperAscii}"
    if not mutation.isEmptyOrWhitespace():
      if mutation notin result:
        result.add mutation
      mutation = ""

proc base(input: string, iVal: var string, start: int): int =
  ## Helper proc to find the base in a Ref-Position-Alt format
  ##
  const bases = {'A', 'C', 'G', 'T', '-'}
  var tmpVal: string = ""
  while result + start < input.len and input[result + start] in bases:
    tmpVal.add input[result + start]
    inc result

  if result > 0:
    iVal = tmpVal

proc baseCmp*(v1, v2: string): int =
  ## A helper proc to sort bases v1 and v2 (format AAA#####BBB)
  ##
  ##   sorted(@[BASE1, BASE2...], cmp=baseCmp)
  ##
  var ref1, ref2, alt1, alt2: string
  var pos1, pos2: int

  if (scanf(v1, "${base}$i${base}", ref1, pos1, alt1) and
      scanf(v2, "${base}$i${base}", ref2, pos2, alt2)):
    if pos1 < pos2: result = -1
    elif pos1 == pos2: result = 0
    else: result = 1

proc parseVariant*(variant: string): tuple[r, a: string, p: int] =
  ## The variants are stored into the DB as A5T (Ref-Position-Alt).
  ## This proc recovers the values from that format, Position is 1-based.
  discard variant.scanf("${base}$i${base}", result.r, result.p, result.a)

proc tableVariants*(vs: seq[string]): Table[int, seq[tuple[r, a: string]]] =
  ## Turn a seq of variants into a table, to speed up searchs
  ##
  var vTuple: tuple[r, a: string, p: int]

  for v in vs:
    vTuple = parseVariant(v)
    if result.hasKeyOrPut(vTuple.p, @[(r: vTuple.r, a: vTuple.a)]):
      result[vTuple.p].add (r: vTuple.r, a: vTuple.a)

proc findDiffs*(align: seq[string], variants: seq[string]): seq[string] =
  ## Return a seq with the Variants found in the align, as REF####ALT
  ##
  ## Asumes the first sequence of the alignment is the Reference, the second
  ##  is the Query
  ##
  const bases = {'A', 'C', 'G', 'T'}
  let vs = tableVariants(variants)
  var idx, idxAbs, span: int = 0

  for base in align[0]:
    if base.toUpperAscii in bases:
      inc idx
    if vs.hasKey(idx):
      for v in vs[idx]:
        span = len(v.r) - 1
        if (v.r == toUpperAscii(align[0][idxAbs .. idxAbs + span]) and
            v.a == toUpperAscii(align[1][idxAbs .. idxAbs + span])):
          result.add toUpperAscii(v.r) & $idx & toUpperAscii(v.a)
    inc idxAbs

proc findDiffs*(align: seq[string], variants: OrderedSet[string]): seq[string] =
  let vs = toSeq(variants)
  findDiffs(align, vs)

proc findDiff*(align: seq[string], variant: string): bool =
  ## Check if "variant" can be found in the align.
  return len(findDiffs(align, @[variant])) > 0

proc diffString*(sequence: string, saveAs: string = ""): seq[string] =
  # Align the bases in `sequence` against the reference
  #
  # If saveAs is given, try to save the alignment to that file
  #
  # Returns the alignment with the Reference
  let bothSequences = preAlign(sequence)

  result = align(bothSequences)
  if not saveAs.isEmptyOrWhitespace():
    let output: File = open(saveAs, fmWrite)
    defer: output.close()

    for sequence in zip(@["Reference", "Query"], result):
      let sr = SequenceRecord(name: sequence[0], record: newDna(sequence[1]))
      sr.dumpTo(output)
