## Grabs an alignment (we use a Gisaid alignment and refined by hand), and
## count all variants found.
##
## Variants are single-letter events, no MNP are considered.
##
## For any variant over two events, the frequency is calculated.
##
## Variants are up-serted into the main DB.
##
import db_sqlite
import os
import sequtils
import strutils
import tables

import bio / fasta
import ../models

const indefs = {'n', 'N', '-'}
const bases = {'A', 'C', 'G', 'T'}
type
  FreqCount = tuple[c: int, v: CountTableRef[string]]

proc newFreqCount: FreqCount =
  result.c = 0
  result.v = newCountTable[string]()

proc variants(refer, query: SequenceRecord): seq[string] =
  ## Find all the differences in Query sequence against the Refer sequence
  ##
  var referPos: int

  for (r, q) in zip(refer.record.chain, query.record.chain):
    if likely(r != '-'):
      referPos.inc
      if likely(r == q):
        continue
      elif q in bases and r in bases:
        result.add r & $referPos & q

proc alignmentVariants(alFile: string, variants: var FreqCount) =
  ## Count the variants in alFile
  ##
  var refer: SequenceRecord

  for query in sequences(alFile):
    if refer.isNil:
      refer = query
      continue
    variants.c.inc

    for variant in variants(refer, query):
      variants.v.inc(variant)

iterator frequencies(account: FreqCount): tuple[v: string, p: float] =
  ## Calculate the frequencies of variants in account.v table, only if the
  ## count is above 1
  ##
  for variant, count in account.v:
    if count > 1:
      yield (v: variant, p: count / account.c)

proc main(alFiles: varargs[string]) =
  var variants: FreqCount = newFreqCount()

  for alFile in alFiles:
    alignmentVariants(alFile, variants)

  var variantId: int64
  getConn

  db.exec(sql"BEGIN TRANSACTION")
  for variant in frequencies(variants):
    variantId = db.insertNewVariant(variant.v[0], variant.v[^1],
                                    parseInt(variant.v[1 .. ^2]))

    doAssert(db.updateVariant(variantId, {"frequency": $variant.p}) == 1)
  db.exec(sql"END TRANSACTION")

when isMainModule:
  if len(commandLineParams()) == 0:
    quit("Need the path of the alignment(s) file. Multiple files allowed")
  main(commandLineParams())
