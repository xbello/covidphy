import os
import strutils

import bio / [sequences, fasta]

type
  Kind* = enum
    kUndefined = "Undefined",
    kMutation = "Mutation",
    kInsertion = "Insertion",
    kDeletion = "Deletion"

  Severity* = enum
    sLow = "Low",
    sMedium = "Medium",
    sHigh = "High"

  Mutation* = tuple
    refer_aa, alt_aa: char
    pos: int
    orf, desc: string
    kind: Kind
    severity: Severity

const refDnaData = slurp(currentSourcePath().parentDir / "resources" /
                         "Reference.fasta")

const orfs: seq[tuple[name: string, start, stop: int]] =
  @[("orf1a", 266, 13468), ("orf1b", 13468, 21555), ("S", 21563, 25384),
    ("orf3a", 25393, 26220), ("E", 26245, 26472), ("M", 26523, 27191),
    ("orf6", 27202, 27387), ("orf7", 27394, 27759), ("orf8", 27894, 28259),
    ("N", 28274, 29533), ("orf10", 29558, 29674)]

proc makeRefDna(): SequenceRecord =
  for sr in sequences(refDnaData.splitLines):
    result = sr
    break

proc setKind(m: var Mutation, r, a: char) =
  ## Side-effect Mutation changing the kind of mutation
  if r != a:
    if r == '-':
      m.kind = kInsertion
    elif a == '-':
      m.kind = kDeletion
    else:
      m.kind = kMutation

proc consequence(mut: var Mutation) =
  ## Side-effect Mutation changing the severity and description.
  ##
  if mut.refer_aa == mut.alt_aa:
    mut.severity = sLow
    mut.desc = "Synonymous"
  else:
    mut.severity = sMedium
    mut.desc = "Missense"
    if mut.alt_aa == '*':
      mut.severity = sHigh
      mut.desc = "Stop gain"
    if mut.refer_aa == '*':
      mut.severity = sHigh
      mut.desc = "Stop loss"
    if mut.refer_aa == 'X' or mut.alt_aa == 'X':
      mut.severity = sHigh
      mut.desc = "Frameshift"
    if mut.pos == 1 and mut.refer_aa == 'M':
      mut.severity = sHigh
      mut.desc = "Start loss"

proc newMutation*(pos: int, base: char): Mutation =
  ## Return a Mutation tuple, with values calculated from the Ref sequence
  ##
  ## Note: pos is 0-index (the first position is 0).
  ##
  ## If the region is outbounds (i.e. not in a ORF), the Mutation is empty:
  ## (refer_aa: '\x00', alt_aa: '\x00', pos: 0, orf: "", desc: "",
  ##  kind: Undefined, severity: Low)
  let refDna = makeRefDna()

  var point: int
  for orf in orfs:
    if pos <= orf.stop and pos >= orf.start:
      result.orf = orf.name
      point = pos - orf.start
      var orfDna: SequenceRecord = refDna[orf.start - 1 .. orf.stop - 1]
      var codon = (orfDna.record).codon(point)
      setKind(result, codon[point mod 3], base)

      # Mutate the reference codon
      result.refer_aa = codon.translate()[0]
      codon[point mod 3] = base
      result.alt_aa = codon.translate()[0]

      result.pos = (point div 3) + 1
      consequence(result)
      return

  setKind(result, refDna[pos], base)

when isMainModule:
  echo newMutation(266, 'G')  # The first base of the first codon
  echo newMutation(14408, 'T')
  echo newMutation(23403, 'G')  # This is the infamous D614G
  #
  echo newMutation(1, 'A')
