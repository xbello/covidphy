##
## Build the needed pathways-marshaled files from CSV data.
##
import algorithm
import bitops
import db_sqlite
import marshal
import math
import os
import parsecsv
import sequtils
import sets
import streams
import strformat
import strscans
import strutils
import sugar
import tables
import bio/fasta
import ../models
import aligner, pathways

const word: int = 64

let reference: SequenceRecord = load(currentSourcePath.parentDir() /
                                     "resources/Reference.fasta")[0]

proc `$`(p: Phylogeny): string =
  result = $p.variants & "\n"
  for k, v in p.signature:
    result.add $k & "\n"
    result.add join(mapIt(v, &"{it.toHex} {it.countSetBits}"), "\n")
    result.add "\n"

proc transInDel(d: string): string =
  ## Someone has the brilliant idea of inventing a new nomenclature without
  ##  telling others. This reverses the idea.
  ##
  var start, stop: int
  if scanf(d, "DEL$i-$i", start, stop):
    return &"{reference[start .. stop].record.chain}{start}{repeat('-', stop - start + 1)}"
  return d

proc transInDels(ds: string): seq[string] =
  ## Parse directly the row, with variants joined with commas
  ##
  mapIt(ds.split(","), it.transInDel)

proc getEvtId(db: DbConn, r: var CsvParser, countryId, evtId: int): int =
  var eventsId: seq[Row]

  eventsId = db.getAllRows(
    sql"""SELECT id FROM superspreader_event
    WHERE country_id = ? AND id = ?""",
    countryId, evtId)

  let msg = &"""{'\n'}Country: {countryId}, Start Date {r.rowEntry("Start s.e.")}, """ &
    &"Event #: {len(eventsId)}"

  doAssert len(eventsId) == 1, msg

  return parseInt(eventsId[0][0])

proc getMutations(csv: string): OrderedSet[string] =
  var mutations: HashSet[string]

  var c: CsvParser
  c.open(csv, separator=';')
  defer: c.close()

  c.readHeaderRow()

  while c.readRow():
    for mutation in transInDels(c.rowEntry("Haplotype")):
      mutations.incl mutation

  toOrderedSet(sorted(toSeq(mutations), baseCmp))

proc addCsv(p: var Phylogeny, f: string, idx: int=1) =
  ## Include CSV in Phylogeny, in order, starting in idx
  getConn

  var c: CsvParser
  c.open(f, separator=';')
  defer: c.close()

  var regionName: string
  var countryId, evtId, rowIdx, startIdx: int
  startIdx = idx

  c.readHeaderRow()
  while c.readRow():
    rowIdx.inc
    startIdx.inc
    regionName = if "State" in c.headers:
      c.rowEntry("State")
    else:
      c.rowEntry("Country")

    countryId = db.getValue(
      sql"""SELECT id FROM country WHERE name = ?""", regionName).parseInt

    try:
      evtId = db.getEvtId(c, countryId, startIdx)
    except AssertionDefect as err:
      echo &"Row: {rowIdx}, Region: {regionName}"
      raise err

    let vars = transInDels(c.rowEntry("Haplotype"))

    p.signature[$evtId] = p.buildSignature(vars)

proc main(fs: varargs[string]) =
  var mutations: OrderedSet[string]
  for f in fs:
    for mutation in getMutations(f):
      mutations.incl mutation

  var phylo = Phylogeny(variants: mutations)
  var idx: int

  for f in fs:
    idx = len(phylo.signature)
    phylo.addCsv(f, idx)

  echo $$phylo

when isMainModule:
  # Run this:
  #  - To build the phylogeny:
  #    nim r buildPathways.py /path/to/1.csv /path/to/2.csv > phylogeny.marshal
  #
  #  - To verify the phylogeny build:
  #    nim r -d:check buildPathways.py /path/to/phylogeny.marshal
  #
  when defined(check):
    block:
      let data = newFileStream(commandLineParams()[0])
      defer: data.close()
      let p = (data.readAll()).to[:Phylogeny]
      echo "Variants #: ", p.variants.card
      echo "Signatures #: ", len(p.signature)
  else:
    main(commandLineParams())
