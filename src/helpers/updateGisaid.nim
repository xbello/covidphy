##
## Updates the pre-built Gisaid classification table with the epicov DB
##
## This script sets the Gisaid pre-classified into haplogroups in the current
##  DB, updating haplogroups where they doesn't exist, and adding the GisaidId
##  to the haplogroup.
##
##  It doesn't update **existent** Gisaid (e.g. if a sequence was classified
##  as A1, and now is classified as A2, it remains A1).
import db_sqlite
import os
import strscans

import ../models

proc haplogroupId(name: string, db: DbConn): string =
  result = db.getValue(sql"SELECT id FROM haplogroup WHERE name=?", name)
  if result == "":
    result = $db.insertID(sql"INSERT INTO haplogroup (name) VALUES (?)", name)

proc main(srcName: string) =
  let srcDb: DbConn = open(srcName, "", "", "")
  defer: srcDb.close

  getConn

  var gisaid: int
  var hap_id: string
  for row in srcDb.rows(sql"SELECT gisaid_id, haplogroup FROM genome"):
    # There is at least one gisaid_id that is MN908947.3
    if likely(scanf(row[0], "$i", gisaid)):

      if db.getValue(sql"SELECT id FROM gisaid WHERE id=?", gisaid) == "":
        # Select-or-Insert the haplogroup Id for this sequence
        hap_id = row[1].haplogroupId(db)

        # This gisaid ID doesn't exist. Insert it
        let new_id = db.tryInsertId(sql"""
          INSERT INTO gisaid (id, haplogroup_id) VALUES (?, ?)""",
          gisaid, hap_id)
        doAssert new_id > 0

when isMainModule:
  if len(commandLineParams()) == 0:
    quit("Need the path to the source Database (with table \"genome\").")
  main(commandLineParams()[0])
