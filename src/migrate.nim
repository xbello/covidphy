import db_sqlite
import macros
import strutils
import tables


proc m_0001(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0001: Create all tables"
  result.add sql"""CREATE TABLE IF NOT EXISTS "results_variants"
    ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
     "result_id" char(32) NOT NULL REFERENCES "result" ("uuid")
       DEFERRABLE INITIALLY DEFERRED,
     "variant_id" integer NOT NULL REFERENCES "variant" ("id")
       DEFERRABLE INITIALLY DEFERRED);"""
  result.add sql"""CREATE TABLE IF NOT EXISTS "result"
    ("uuid" char(32) NOT NULL PRIMARY KEY,
     "status" smallint unsigned NOT NULL CHECK ("status" >= 0),
     "created" date NOT NULL);"""
  result.add sql"""CREATE TABLE IF NOT EXISTS "variant"
    ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
     "alt" varchar(1) NOT NULL,
     "ref" varchar(1) NOT NULL,
     "ref_aa" varchar(1),
     "alt_aa" varchar(1),
     "position" integer unsigned NULL CHECK ("position" >= 0),
     "position_aa" integer unsigned NULL CHECK ("position_aa" >= 0),
     "orf_name" varchar(16),
     "kind" smallint unsigned NOT NULL CHECK ("kind" >= 0),
     "severity" smallint unsigned NOT NULL CHECK ("severity" >= 0),
     "description" varchar(64) NOT NULL,
     "frequency" decimal NULL,
     UNIQUE(alt, ref, position));"""
  result.add sql"""CREATE INDEX IF NOT EXISTS "aligner_var_pos_idx"
    ON "variant" ("position", "ref", "alt")"""

  result.add sql"""CREATE TABLE IF NOT EXISTS "country"
    ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
     "name" char(32) NOT NULL,
     "latitude" decimal NOT NULL,
     "longitude" decimal NOT NULL)"""
  result.add sql"""CREATE TABLE IF NOT EXISTS "haplotype"
    ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" char(16) NOT NULL)"""
  result.add sql"""CREATE TABLE IF NOT EXISTS "countries_haplotypes"
    ("id_country" integer NOT NULL REFERENCES "country" ("id")
       DEFERRABLE INITIALLY DEFERRED,
     "id_haplotype" integer NOT NULL REFERENCES "haplotype" ("id")
       DEFERRABLE INITIALLY DEFERRED,
     "count" int unsigned NOT NULL CHECK ("count" >= 0))"""
  result.add sql"""CREATE INDEX IF NOT EXISTS "country_idx"
    ON "country" ("id")"""
  result.add sql"""CREATE INDEX IF NOT EXISTS "haplotype_idx"
    ON "haplotype" ("id")"""

proc m_0002(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0002: Add column 'continent' to table 'country'"
  result.add sql"""ALTER TABLE country
    ADD COLUMN 'continent' varchar(64)"""

proc m_0003(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0003: Rename everything from 'haplotype' to 'haplogroup'"
  result.add sql"""DROP INDEX IF EXISTS "haplotype_idx" """
  result.add sql"""ALTER TABLE "haplotype" RENAME TO "haplogroup" """
  result.add sql"""CREATE INDEX IF NOT EXISTS
    "haplogroup_idx" ON "haplogroup" ("id")"""
  result.add sql"""ALTER TABLE countries_haplotypes
    RENAME TO countries_haplogroups"""
  result.add sql"""ALTER TABLE countries_haplogroups
    RENAME COLUMN id_haplotype TO id_haplogroup"""

proc m_0004(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0004: Add the table Gisaid"
  result.add sql"""CREATE TABLE IF NOT EXISTS "gisaid"
    ("id" integer UNIQUE NOT NULL PRIMARY KEY,
     "haplogroup_id" integer NOT NULL REFERENCES "haplogroup" ("id")
       DEFERRABLE INITIALLY DEFERRED)"""

proc m_0005(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0005: Change the country column 'continent' to a self-reference"
  result.add sql"PRAGMA foreign_keys=off"
  result.add sql"BEGIN TRANSACTION"
  result.add sql"""CREATE TABLE IF NOT EXISTS "country_tmp"
    ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
     "name" char(32) NOT NULL,
     "latitude" decimal NOT NULL,
     "longitude" decimal NOT NULL,
     "continent" integer REFERENCES "country_tmp" ("id") DEFAULT NULL
     DEFERRABLE INITIALLY DEFERRED)"""
  result.add sql"""INSERT INTO "country_tmp"
    SELECT "id", "name", "latitude", "longitude", "continent"
    FROM country"""
  result.add sql"""DROP TABLE country"""
  result.add sql"""ALTER TABLE country_tmp RENAME TO country"""
  result.add sql"""END TRANSACTION"""
  result.add sql"PRAGMA foreign_keys=on"

proc m_0006(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0006: Add column 'stats' to results"
  result.add sql"BEGIN TRANSACTION"
  result.add sql"""ALTER TABLE result
    ADD COLUMN 'stats' TEXT"""
  result.add sql"END TRANSACTION"

proc m_0007(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0007: Add columns 'name' and 'group' to results"
  result.add sql"BEGIN TRANSACTION"
  result.add sql"""ALTER TABLE result
    ADD COLUMN 'name' char(32)"""
  result.add sql"""ALTER TABLE result
    ADD COLUMN 'group' char(32)"""
  result.add sql"END TRANSACTION"

proc m_0008(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0008: Add table 'concern_variant'"
  result.add sql"BEGIN TRANSACTION"

  result.add sql"""CREATE TABLE IF NOT EXISTS "concern_variant"
    ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
     "base" varchar(32) NOT NULL,
     "aa" varchar(32),
     UNIQUE(base));"""

  result.add sql"END TRANSACTION"

proc m_0009(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0009: Link M2M 'concern_variant' with 'result'"

  result.add sql"BEGIN TRANSACTION"

  result.add sql"""CREATE TABLE IF NOT EXISTS "results_concernvariants"
     ("result_id" char(32) NOT NULL REFERENCES "result" ("uuid")
       DEFERRABLE INITIALLY DEFERRED,
     "concernvariant_id" integer NOT NULL REFERENCES "concern_variant" ("id")
       DEFERRABLE INITIALLY DEFERRED);"""

  result.add sql"END TRANSACTION"

proc m_0010(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0010: Add 'superspreader_event' table, linked M2M with 'country'"

  result.add sql"BEGIN TRANSACTION"

  result.add sql"""
    CREATE TABLE IF NOT EXISTS "superspreader_event"
      ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
       "start_date" date NOT NULL,
       "end_date" date NOT NULL,
       "first_date" date NOT NULL,
       "last_date" date NOT NULL,
       "event_sequences" integer unsigned,
       "event_total_sequences" integer unsigned,
       "n_sequences" integer unsigned,
       "total_sequences" integer unsigned,
       "near_sequences" integer unsigned,
       "image" char(255));"""

  result.add sql"""CREATE TABLE IF NOT EXISTS "seevents_countries"
     ("seevent_id" char(32) NOT NULL REFERENCES "superspreader_event" ("id")
       DEFERRABLE INITIALLY DEFERRED,
      "country_id" integer NOT NULL REFERENCES "country" ("id")
       DEFERRABLE INITIALLY DEFERRED);"""

  result.add sql"END TRANSACTION"

proc m_0011(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0011: Add 'results_seevents' table, linking M2M 'result' with "
    echo "      'superspreader_event'."

  result.add sql"BEGIN TRANSACTION"

  result.add sql"""CREATE TABLE IF NOT EXISTS "results_seevents"
     ("seevent_id" integer NOT NULL REFERENCES "superspreader_event" ("id")
       DEFERRABLE INITIALLY DEFERRED,
      "result_id" char(32) NOT NULL REFERENCES "result" ("uuid")
       DEFERRABLE INITIALLY DEFERRED);"""

  result.add sql"END TRANSACTION"

proc m_0012(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0012: Create index for 'gisaid' table, it got bigger."

  result.add sql"BEGIN TRANSACTION"
  result.add sql"""CREATE INDEX IF NOT EXISTS "gisaid_idx"
    ON "gisaid" ("id")"""
  result.add sql"END TRANSACTION"

proc m_0013(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0013: Add 'seevents_haplogroups' table, linking M2M "
    echo "      'superspreader_event' with 'haplogroup'."

  result.add sql"BEGIN TRANSACTION"

  result.add sql"""CREATE TABLE IF NOT EXISTS "seevents_haplogroups"
     ("seevent_id" integer NOT NULL REFERENCES "superspreader_event" ("id")
       DEFERRABLE INITIALLY DEFERRED,
      "haplogroup_id" integer NOT NULL REFERENCES "haplogroup" ("id")
       DEFERRABLE INITIALLY DEFERRED);"""

  result.add sql"END TRANSACTION"

proc m_0014(db: DbConn, verbose: bool): seq[SqlQuery] =
  if verbose:
    echo "0014: Remove M2M between 'superspreader_event' and 'haplogroup'."
    echo "      Remove M2M between 'superspreader_event' and 'country'."
    echo "      Add O2M between 'superspreader_event' and 'haplogroup'."
    echo "      Add O2M between 'superspreader_event' and 'country'."

  result.add sql"BEGIN TRANSACTION"

  result.add sql"""DROP TABLE IF EXISTS "seevents_haplogroups";"""
  result.add sql"""DROP TABLE IF EXISTS "seevents_countries";"""
  result.add sql"""ALTER TABLE "superspreader_event"
    ADD COLUMN "country_id" INTEGER REFERENCES "country" ("id")
    DEFERRABLE INITIALLY DEFERRED"""
  result.add sql"""ALTER TABLE "superspreader_event"
    ADD COLUMN "haplogroup_id" INTEGER REFERENCES "haplogroup" ("id")
    DEFERRABLE INITIALLY DEFERRED"""

  result.add sql"END TRANSACTION"

var m = initOrderedTable[string, proc(db: DbConn, verbose: bool):
  seq[SqlQuery]{.nimcall.}]()
const finish = 14  # The last migration in the file

macro loadMigrations: untyped =
  ## Load the migration procedures from 1 to "finish" into m
  result = nnkStmtList.newTree()

  var migration: string
  for n in 1 .. finish:
    migration = align($n, 4, '0')  # Creates the padded migration id
    result.add nnkAsgn.newTree(
      nnkBracketExpr.newTree(
        newIdentNode("m"),
        newLit(migration)
      ),
      newIdentNode("m" & migration)
    )

proc migrate*(db: DbConn, verbose: bool = false) =
  loadMigrations()
  for migration in m.keys():
    for step in m[migration](db, verbose):
      db.exec(step)

when isMainModule:
  import os
  import models

  block:
    let migration: string = commandLineParams()[0]
    getConn
    if migration == "all":
      db.migrate(verbose=true)
    else:
      loadMigrations()
      for step in m[migration](db, verbose=true):
        db.exec(step)
