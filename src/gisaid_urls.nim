import json
import strformat

import jester

import ./forms
import ./utils
import ./views
import helpers / csrf

router gisaid:
  # Gisaid search form
  get "":
    redirect "/gisaid/"
  get "/":
    let secret = newSecret()
    setToken secret
    getErrors
    resp gisaidForm(secret, errors)
  post "/":
    if checkToken(request):
      let (haplogroupId, err) = processGisaid(@"gisaidId")
      if len(err) > 0:
        setCookie("formErrors", $(%*err))
        redirect "/gisaid/"
      redirect &"/phylogeny/{haplogroupId}/"
    else:
      halt Http403
