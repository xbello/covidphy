import jester

import ./views

router superspreader:
  # Superspreaders views
  get "":
    redirect "/superspread/"
  get "/":
    resp superspreadersList()
  get "/@id":
    redirect "/superspread/" & @"id" & "/"
  get "/@id/":
    let r = superspreaderDetail(@"id")
    resp r.code, r.content
