import ./genbank_urls
import ./gisaid_urls
import ./phylogeny_urls
import ./result_urls
import ./superspreader_urls

export genbank_urls
export gisaid_urls
export phylogeny_urls
export result_urls
export superspreader_urls

{.used.}
