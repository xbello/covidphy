import std / [os, sets, strformat, strutils]
import bio / fasta
import nigui, nigui/msgbox

import ../ src / helpers / [aligner, pathways]

const resources = currentSourcePath().splitPath.head / "resources"
const icon = resources / "logoSquareDark.png"
const version = strip(staticRead("VERSION"))

proc main =
  app.init()
  var window = newWindow(&"CovidPhy - {version}")
  window.iconPath = icon

  # Action buttons
  var loadFastaBtn = newButton("load Fasta...")
  var analizeBtn = newButton("Analize")
  analizeBtn.enabled = false
  var abortBtn = newButton("Abort")
  abortBtn.enabled = false
  var saveBtn = newButton("Save")
  saveBtn.enabled = false

  # Text area
  var progressTextArea = newTextArea()

  # Status bar
  var totalSeqsLbl = newLabel("Total Sequences: ")
  var totalSeqsCount = newLabel("0")
  var totalHaplosLbl = newLabel("Total Haplogroups: ")
  var totalHaplosCount = newLabel("0")
  var progressBar = newProgressBar()
  progressBar.value = 0.0

  # Globals
  var fileSize, analyzedSize: int
  var abort: bool = false
  var filePath: string
  var outputContent: seq[string]

  # Button bindings
  proc loadFile(event: ClickEvent) =
    var dialog = newOpenFileDialog()
    dialog.title = "Load a Fasta file"
    dialog.directory = "./"
    dialog.run()

    if len(dialog.files) > 0:
      let tmp = dialog.files[0]
      if toLowerAscii(tmp.splitFile.ext) in [".fas", ".fst", ".fasta"]:
        filePath = tmp
        analizeBtn.enabled = true
        fileSize = int(getFileInfo(filePath).size)
      else:
        discard window.msgBox(&"{tmp}\nNot a valid Fasta file name. Must " &
                              "end in .fas, .fst or .fasta")

  proc abortAnalisys(event: ClickEvent) =
    abort = true

  proc analize(event: ClickEvent) =
    var i: int
    var haplogroupsFound = initHashSet[string]()
    var haplogroupsClassified: seq[string]

    analizeBtn.enabled = false
    abortBtn.enabled = true
    progressTextArea.text = ""
    progressTextArea.addLine "Sequence Id\tHaplogroup"

    for sr in sequences(filePath):
      inc i
      haplogroupsClassified = classify(phylogeny,
                                       diffs(diffString(sr.record.chain)))
      haplogroupsFound.incl(join(haplogroupsClassified, "/"))
      progressTextArea.addLine &"{sr.name}\t{join(haplogroupsClassified, \"/\")}"
      outputContent.add &"{sr.name}\t{join(haplogroupsClassified, \"/\")}"
      progressTextArea.scrollToBottom()
      totalSeqsCount.text = $i
      totalHaplosCount.text = $len(haplogroupsFound)
      analyzedSize.inc len(sr)
      progressBar.value = clamp(analyzedSize / fileSize, 0.0, 1.0)

      if abort:
        abort = false
        break

    analizeBtn.enabled = true
    abortBtn.enabled = false
    saveBtn.enabled = true

  proc saveOutput(event: ClickEvent) =
    let saveFilePath = newSaveFileDialog()
    saveFilePath.title = "Save results as TSV"
    saveFilePath.directory = "./"
    saveFilePath.defaultExtension = ".tsv"
    saveFilePath.run()

    let outputFile = open(saveFilePath.file, fmWrite)
    defer: outputFile.close
    for line in outputContent:
      outputFile.writeLine(line)
    discard window.msgBox(&"Saved {saveFilePath.file}.")

  ## Layout building
  var mainContainer = newLayoutContainer(Layout_Vertical)

  var mainActions = newLayoutContainer(Layout_Horizontal)
  mainActions.spacing = 15
  mainActions.padding = 15

  var progressText = newLayoutContainer(Layout_Horizontal)

  var statusInfo = newLayoutContainer(LayoutHorizontal)
  statusInfo.spacing = 15
  statusInfo.padding = 15

  statusInfo.add(totalSeqsLbl)
  statusInfo.add(totalSeqsCount)
  statusInfo.add(totalHaplosLbl)
  statusInfo.add(totalHaplosCount)
  statusInfo.add(progressBar)

  progressText.add(progressTextArea)

  loadFastaBtn.onClick = loadFile
  analizeBtn.onClick = analize
  abortBtn.onClick = abortAnalisys
  saveBtn.onClick = saveOutput
  mainActions.add(loadFastaBtn)
  mainActions.add(analizeBtn)
  mainActions.add(abortBtn)
  mainActions.add(saveBtn)

  mainContainer.add(mainActions)
  mainContainer.add(progressText)
  mainContainer.add(statusInfo)

  ## Show
  window.add(mainContainer)
  window.show()
  app.run()

when isMainModule:
  main()
