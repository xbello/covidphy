import httpclient
import strformat
import strutils

import karax / [karaxdsl, vdom]

import data_clean
import models
import helpers / entrezController

type
  ErrorLevel* = enum
    elInfo = "info"
    elWarning = "warning"
    elDanger = "danger"
  FormError* = object of RootObj
    error*, message*: string
    field*: string  # This matches with the field id attr, to highlight it
    level*: ErrorLevel

proc toggleClass*(class, toggle: string): string =
  ## Returns a string with the class added/removed toggle string
  ## TODO: This is a helper proc, and should be in other module
  var classes = class.splitWhitespace()

  if toggle in classes:
    classes.del(classes.find(toggle))
  else:
    classes.add toggle

  join(classes, " ")

proc addClass*(classes: var string, addend: string) =
  ## Return a string with the class added if it's new
  ## TODO: This is a helper proc, and should be in other module
  ##
  if addend notin classes:
    classes.add " " & addend

proc markErrors*(form: VNode, errors: seq[FormError]) =
  ## Insert the message boxes above the form if errors exist.
  ## Add the class "is-invalid" to any field in 'form' that is marked in 'errors'
  ## TODO: This is a helper proc, and should be in other module
  ##
  var invalid: VNode
  var fields: seq[string]

  if len(errors) > 0:
    let messages = buildHtml(tdiv(class="my-2")):
      for e in errors:
        tdiv(class= &"alert alert-{e.level}"):
          strong:
            text e.error & ": "
          verbatim e.message

    form.insert(messages, 0)

  for e in errors:
    if e.field notin fields:
      fields.add e.field
      invalid = form.getVNodeById(e.field)
      if not invalid.isNil:
        invalid.class.addClass("is-invalid")

proc isEmpty(query, fieldId: string, errors: var seq[FormError]): bool =
  if query == "":
    errors.add FormError(
      level: elDanger,
      error: "Empty",
      field: fieldId,
      message: "The search box cannot be empty.")
    return true

proc maxLength*(query: string, limit: int): bool =
  len(query) <= limit

proc minLength*(query: string, limit: int): bool =
  len(query) >= limit

proc isGisaidValid*(gisaidId: string): seq[FormError] =
  var gisaidInt: int

  if isEmpty(gisaidId, "id_gisaid", result): return

  try:
    gisaidInt = parseInt(gisaidId)
  except ValueError:
    result.add FormError(
      level: elDanger,
      error: "Not valid",
      field: "id_gisaid",
      message: "The ID must follow the format EPI_ISL_NNN, NNN being a " &
        &"number. {gisaidInt} doesn't seem a valid ID.")
    return

  let range = selectMinMaxGisaid()
  if gisaidInt < range.minV or gisaidInt > range.maxV:
    result.add FormError(
      level: elWarning,
      error: "Out of range",
      field: "id_gisaid",
      message: &"The ID {gisaidId} seems valid but we only have sequences " &
               &"{range.minV} to {range.maxV} in our DB.")

  if selectHaplogroupByGisaid($gisaidInt) == "":
    result.add FormError(
      level: elWarning,
      error: "Not in the DB",
      field: "id_gisaid",
      message: &"The ID {gisaidId} is not in our DB.")

proc isValid*(sequence: string): seq[FormError] =
  ## Checks if the sequence submited meets certain requisites.
  ##
  if not minLength(sequence, 1 shl 14):
    result.add FormError(
      level: elDanger,
      error: "Too short",
      field: "id_sequence",
      message: &"The sequence must be at least {1 shl 14} (2<sup>14</sup>) bases.")

proc idIsValid*(idString: string): seq[FormError] =
  ## Check if idString can be parsed into two identifiers: group and sequenceId
  ##
  let data = cleanId(idString)
  let limit = 32

  if not maxLength(data.group, limit):
      result.add FormError(
        level: elDanger,
        error: "Too long",
        field: "id_sample",
        message: &"Group Id is too long (> {limit} chars)")
  if not maxLength(data.name, limit):
      result.add FormError(
        level: elDanger,
        error: "Too long",
        field: "id_sample",
        message: &"Sequence Id is too long (> {limit} chars)")

proc isGenbankValid*(genbankId: string, seqChain: var string): seq[FormError] =
  if isEmpty(genbankId, "id_genbank", result): return

  try:
    seqChain = getGenbankSeq(genbankId)
  except HttpRequestError:
    result.add FormError(
      level: elDanger,
      error: "Request Error",
      field: "id_genbank",
      message: "There was an internal error while retrieving "&
      &"<strong>{genbankId}</strong>. " &
      "There's nothing you can do except re-check the ID is correct.")
    return

  seqChain = cleanSequence(seqChain)

  isValid(seqChain)

proc processGisaid*(id: string): tuple[id: string, errors: seq[FormError]] =
  ## Return the haplogroup name for a given Gisaid Id
  ##
  let gisaidId = cleanGisaidId(id)
  result.errors = isGisaidValid(gisaidId)

  if len(result.errors) > 0:
    return

  result.id = selectHaplogroupByGisaid(gisaidId)

proc processSequence*(sequence, save, name: string):
  tuple[id: string, errors: seq[FormError]] =
  var sequence = cleanSequence(sequence)

  result.errors = isValid(sequence)

  if len(result.errors) > 0:
    return

  let saveFile = if @"saveAlignment" == "on": true else: false
  result.id = alignMafft(sequence, saveFile=saveFile)
  discard addResultName(result.id, name)
  discard updateStats(result.id, sequenceStats(sequence))
