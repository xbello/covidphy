import json
import strformat
import strutils

import jester

import ./forms
import ./models
import ./utils
import ./views
import helpers / csrf


router result:
  get "":
    redirect "/align/"
  # Align -> Result path
  get "/":
    let secret = newSecret()
    setToken secret
    getErrors
    resp alignForm(secret, errors)
  post "/":
    if checkToken(request):
      let (uuid, err) = processSequence(
        @"sequence", @"saveAlignment", @"id_sample")
      if len(err) > 0:
        setCookie("formErrors", $(%*err))
        redirect "/align/"
      redirect &"/align/result/{uuid}/"
    else:
      halt Http403
  get "/result/@id/json":
    if not resultExists(@"id"):
      halt Http404
    resp resultJson(@"id")
  get "/result/@id":
    redirect "/align/result/" & @"id" & "/"
  get "/result/@id/":
    if not resultExists(@"id"):
      halt Http404
    resp resultDetail(@"id")
  get "/result/@id/group":
    redirect "/align/result/" & @"id" & "/group/"
  get "/result/@id/group/":
    if not resultExists(@"id"):
      halt Http404
    resp resultGroup(@"id")
