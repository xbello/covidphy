import httpcore
import logging
import marshal
import os
import smtp
import strformat

when isMainModule:
  import dotenv

type
  MailLogger* = ref object of Logger
    msgTo: seq[string]
    smtpConn: Smtp

let defaultLevel = getEnv("logLevel")
let minLevel = to[Level](&""""{defaultLevel}"""")

proc newMailLogger*(levelThreshold = lvlError): MailLogger =
  new result
  result.levelThreshold = levelThreshold
  result.msgTo = @[getEnv("mailTo")]

  let smtpConn = newSmtp(useSsl=true)
  smtpConn.connect("smtp.gmail.com", Port 465)
  smtpConn.auth(getEnv("mailFrom"), getEnv("mail"))
  result.smtpConn = smtpConn

method log*(logger: MailLogger, level: Level, args: varargs[string, `$`]) =
  if level >= minLevel and level >= logger.levelThreshold:
    try:
      var body: string
      for arg in args:
        body.add arg & "\n"

      var msg = createMessage(
        &"{LevelNames[level]} at covidphy.eu", body, logger.msgTo)
      logger.smtpConn.sendmail(getEnv("mailFrom"), logger.msgTo, $msg)
    except:
      discard

when isMainModule:
  let env = initDotEnv()
  env.load()

  # var mLogger = newMailLogger(levelThreshold=lvlAll)
  var mLogger = newMailLogger()
  addHandler(mLogger)

  log(level=lvlFatal, Http500, "Level Fatal")
  log(level=lvlWarn, Http404, "Level Warn")
  log(level=lvlDebug, Http200, "Level Debug")

  fatal("Level Fatal")
  warn("Level Warn")
  debug("Level Debug")
