import strtabs
import karax / [karaxdsl, vdom, vstyles]

import base
import icons

type
  Box = object of RootObj
    href, bgImage, title, desc: string

proc leftAbout: VNode =
  ## The left slanted animated square
  buildHtml(tdiv(class="left-fold")):
    tdiv(class="left-fold-bg bg1")
    tdiv(class="left-fold-bg bg1 bg2")
    tdiv(class="left-fold-bg bg1 bg3")
    tdiv(class="container h-100"):
      tdiv(class="row h-100"):
        tdiv(class="col-12 my-auto"):
          tdiv(class="left-content text-white py-5 py-md-0"):
           img(src=static("img/logoFullDark.png"))

proc boxNode(box: Box): VNode =
  buildHtml(tdiv(class="col-lg-4 col-sm-6")):
    a(class="boxed", href=box.href, target="_blank"):
      img(class="img-fluid", src=static(box.bgImage))
      tdiv(class="boxed-caption"):
        tdiv(class="boxed-title text-white-50"):
          text box.title
        tdiv(class="boxed-desc"):
          text box.desc

proc softNode(logo: Box): Vnode =
  ## Creates a linked image
  buildHtml(tdiv(class="col-2")):
    a(class="boxed", href=logo.href):
      img(class="img-fluid", src=static(logo.bgImage), alt=logo.title,
          style=style(StyleAttr.width, "100%"))
      tdiv(class="boxed-caption", style=style(StyleAttr.left, "-1px")):
        tdiv(class="boxed-title text-white-50"):
          text logo.title

proc cites: VNode =
  ## The publications to cite
  ##
  buildHtml(tdiv):
    tdiv:
      p:
        text "Alberto Gómez-Carballa, Xabier Bello, Jacobo Pardo-Seco, " &
             "Federico Martinón-Torres and Antonio Salas (2020). "
        a(href="http://doi.org/10.1101/gr.266221.120", target="_blank"):
          svg(class="i"):
            verbatim "<use xlink:href=\"#i-link\">"
          text "Mapping genome variation of SARS-CoV-2 worldwide " &
            "highlights the impact of COVID-19 super-spreaders. "
        em:
          text "Genome Research."
    tdiv:
      p:
        text "Alberto Gómez-Carballa, Xabier Bello, Jacobo Pardo-Seco, " &
             "María Luisa Pérez del Molino, " &
             "Federico Martinón-Torres and Antonio Salas (2020). "
        a(href="https://doi.org/10.24272/j.issn.2095-8137.2020.217",
          target="_blank"):
          svg(class="i"):
            verbatim "<use xlink:href=\"#i-link\">"
          text "Phylogeography of the SARS-CoV-2 pandemic in Spain: a " &
               "story of multiple introductions, micro-geographic " &
               "stratification, founder effects and super-spreaders. "
        em:
          text "Zoological Research."
    tdiv:
      p:
        text "Alberto Gómez-Carballa, Xabier Bello, Jacobo Pardo-Seco, " &
             "Federico Martinón-Torres and Antonio Salas (2021). "

        a(href="https://doi.org/10.24272/j.issn.2095-8137.2020.364",
          target="_blank"):
          svg(class="i"):
            verbatim "<use xlink:href=\"#i-link\">"
          text "Pitfalls of barcodes in the study of worldwide SARS-CoV-2 " &
                "variation and phylodynamics. "
        em:
          text "Zoological Research."
    tdiv:
      p:
        text "Alberto Gómez-Carballa, Xabier Bello, Jacobo Pardo-Seco, " &
             "Federico Martinón-Torres and Antonio Salas (2021). "

        a(href="https://science.sciencemag.org/content/early/2020/12/09/" &
          "science.abe3261/tab-e-letters", target="_blank"):
          svg(class="i"):
            verbatim "<use xlink:href=\"#i-link\">"
          text "Superspreading: The engine of the SARS-CoV-2 pandemic. "
        em:
          text "Science, eLetter."

proc rightAbout: VNode =
  ## The content in the right
  ##
  let boxes: seq[Box] = @[
    Box(href: "http://www.obrasocialpediatria.org/como-colaborar.html",
        bgImage: "img/LogoObraSocial.png",
        title: "Obra Social Pediatría",
        desc: "Thank us donating towards children research"),
    Box(href: "http://www.genpob.eu/",
        bgImage:"img/LogoGenpob.png",
        title:"GenPob",
        desc:"Visit our group webpage"),
    Box(href:"http://www.genvip.eu/",
        bgImage:"img/LogoGenvip.png",
        title:"GenVIP",
        desc:"Visit our main group webpage")]

  let softwares: seq[Box] = @[
    Box(href: "https://www.nim-lang.org",
        bgImage: "img/logoNim.png",
        title: "Nim"),
    Box(href: "https://github.com/dom96/jester",
        bgImage: "img/logoJester.png",
        title: "Jester"),
    Box(href: "https://mafft.cbrc.jp/",
        bgImage: "img/logoAnonymous.svg",
        title: "MAFFT"),
    Box(href: "https://plotly.com/",
        bgImage: "img/logoPlotly.png",
        title: "Plotly"),
    Box(href: "https://getbootstrap.com/",
        bgImage: "img/logoBootstrap.png",
        title: "Bootstrap")]

  buildHtml(tdiv(class="container h-100")):
    tdiv(class="row h-100"):
      tdiv(class="col-12 my-auto"):
        tdiv(class="mb-4"):
          h4:
            text "Cite us if you used this app"
          p:
            text "Someday, when this is done and published..."  # TODO
        tdiv(class="my-4"):
          h4:
            text "Other references"
          cites()
        h4:
          text "Some links you might like:"
        tdiv(class="mt-4"):
          tdiv(class="container-fluid p-0"):
            tdiv(class="row no-gutters"):
              for box in boxes:
                boxNode(box)
        h4(class="mt-4"):
          text "Stack used to build this page:"
        tdiv(class="container-fluid"):
          tdiv(class="row"):
            for software in softwares:
              softNode(software)

proc aboutTemplate*(): string =
  let node = buildHtml(tdiv(class="container")):
    verbatim iconsSvg()
    leftAbout()
    tdiv(class="right-fold"):
      rightAbout()

  var context = {"active": "About"}.newStringTable
  baseHtml(node, context=context)
