import dom

proc hideShowArrow(ev: Event) =
  let arrow = document.querySelector("div.keepreading")
  let lastSection = document.getElementById("section_last")

  let cls: string = "notvisible"

  if lastSection.getBoundingClientRect().top <= 0: # S_last is in viewport
    if not contains(arrow.classList, cls):         # Then make arrow invisible
      arrow.classList.add(cls)
  else:                                # S_last is not in viewport
    if contains(arrow.classList, cls):
      arrow.classList.remove(cls)      # Make the arrow visible

proc onLoad() {.exportc.} =
  let infos = document.querySelector("div.section-holder")
  infos.addEventListener("scroll", hideShowArrow)
