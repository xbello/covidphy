import dom


proc highlight(ev: Event) =
  for attr in ev.target.attributes:
    if attr.nodeName == "data-toggle":
      var el: Element = document.getElementById(attr.nodeValue)
      el.scrollIntoView
      el.classList.toggle("bg-danger")

proc onLoad() {.exportc.} =
  for btn in document.querySelectorAll(".toggling-btn"):
    btn.addEventListener("click", highlight)
