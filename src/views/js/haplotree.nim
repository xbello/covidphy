import dom


proc folding(ev: Event) =
  for child in ev.target.children:
    if child.nodeName == "use":
      for attr in child.attributes:
        if attr.nodeName == "xlink:href":
          if attr.nodeValue == "#i-plus":
            attr.nodeValue = "#i-minus"
          else:
            attr.nodeValue = "#i-plus"
          break

  for child in ev.target.parentNode.parentNode.parentNode.children:
    if child.nodeName == "UL":
      if child.class == "a__animated a__fadeOut folded":
        child.class = "a__animated a__fadeIn"
      else:
        child.class = "a__animated a__fadeOut folded"
      break

proc onLoad() {.exportc.} =
  for node in document.querySelectorAll(".tree li.parent_li > span > div"):
    node.addEventListener("click", folding)
