import dom, math, random, strutils, sugar

proc redirectToSample(ev: Event) =
  window.location.pathname = "align/result/00000000-0000-0000-0000-000000000000/"

proc loadSequence(ev: Event) =
  let info = "Yeah, you noticed: this is not a real sequence, but a random "&
             "one plus a redirection to the real results. Saving 32kb of " &
             "dead weight here."
  let seqTextarea = document.getElementById("id_sequence")
  let submitBtn = document.getElementById("submit-id-submit")

  let bases = {'A', 'C', 'G', 'T'}
  let length = 2^15
  let rndSeq = collect(newSeqOfCap(length)):
    for i in 1 .. length:
      sample(bases)
  seqTextarea.value = ">MZ350188.1\n" & join(rndSeq)

  submitBtn.addEventListener("click", redirectToSample)
  submitBtn.setAttribute("type", "button")

proc resetSubmit(ev: Event) =
  ## Put the Submit button in its default state, removing redirection.
  ##
  let submitBtn = document.getElementById("submit-id-submit")
  submitBtn.setAttribute("type", "submit")
  submitBtn.removeEventListener("click", loadSequence)

proc onLoad() {.exportc.} =
  let link = document.getElementById("sampleSequence")
  link.addEventListener("click", loadSequence)

  let reset = document.getElementById("reset-id-reset")
  reset.addEventListener("click", resetSubmit)
