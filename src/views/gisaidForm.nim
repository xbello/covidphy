import strformat
import strtabs
import karax / [karaxdsl, vdom]

import base
import .. / forms
import .. / models
import .. / helpers / csrf

proc pageText: VNode =
  let count: int = selectCountGisaid()
  let range: tuple[minV, maxV: int] = selectMinMaxGisaid()
  buildHtml(tdiv(class="my-3")):
    h3:
      text "Find the haplogroup for a "
      a(href="https://www.gisaid.org/", target="_new"):
        text "Gisaid"
      text " sequence"
    tdiv:
      p:
        text &"""We have already pre-classified {count} sequences, with IDs
        from EPI_ISL_{range.minV} to EPI_ISL_{range.maxV}, so you don't need to
        copy/paste them to know their haplogroup.

        Some sequences in the range are missing because its ID doesn't exist or
        the sequence has been taken down."""

proc gisaidForm*(csrfSecret: string, errors: seq[FormError] = @[]): string =
  let node = buildHtml(tdiv(class="col")):
    pageText()
    form(`method`="post"):
      tdiv:
        input(name="CSRFToken", type="hidden", value=maskToken(csrfSecret))
      tdiv(id="div_id_gisaid", class="form-inline"):
        tdiv(class="input-group w-75 mx-auto"):
          input(name="gisaidId", class="form-control", id="id_gisaid",
                placeholder="E.g. EPI_ISL_402125 or 402125")
          tdiv(class="input-group-append"):
            button(type="submit", name="submit", value="Submit",
                   class="btn btn-primary", id="submit-id-submit"):
              text "Search"

  markErrors(node, errors)

  var context = {"active": "Gisaid Search"}.newStringTable
  baseHtml(node, context=context)
