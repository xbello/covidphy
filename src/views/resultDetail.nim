import algorithm
import json
import os
import sequtils
import strformat
import strtabs
import strutils
import times
import unicode

import dotenv
import karax / [karaxdsl, vdom, vstyles]

import base
import plotly_maps
import sars_genome
import ../ models
import ../ helpers / pathways

let env = initDotEnv()
env.load()

proc sortedVariants(vs: seq[string]): seq[string] =
  proc cmpV(x, y: string): int =
    if x[1 .. ^2].parseInt >= y[1 .. ^2].parseInt:
      1
    else:
      -1
  result = sorted(vs, cmpV)

proc concernButtons*(variants: seq[StringTableRef]): VNode =
  buildHtml(tdiv):
    if len(variants) > 0:
      p:
        text "Your sequence contains the following variants of concern:"
      span:
        for variant in variants:
          button(`type`="button", class= &"btn btn-warning",
                 style=style(StyleAttr.margin, "3px")):
            text variant["base"]
            if not isEmptyOrWhitespace(variant["aa"]):
              text " (" & variant["aa"] & ")"

proc pathwayButtons(variants, pathway: seq[string], color: string = "info"):
  VNode =

  var variantsInPathway: seq[string] = filterIt(variants, it in pathway)

  buildHtml(span):
    for variant in sortedVariants(variantsInPathway):
      if color == "info":
        button(`type`="button", class= &"btn btn-{color} toggling-btn",
               `data-toggle`= &"{variant}",
               style = style(StyleAttr.margin, "0 3px")):
          text variant
      else:
        button(`type`="button", class= &"btn btn-{color}",
               style = style(StyleAttr.margin, "0 3px")):
          text variant

proc tableVariant(data: seq[Variant], color: string = "info"): VNode =
  if len(data) == 0:
    return buildHtml(tdiv):
      p:
        text "No variants found in this sequence"

  buildHtml(tdiv):
    tdiv(class="mt-2"):
      p:
        text "This is a table with all the variants found in your sequence."
        if len(data) > 499:
          strong(class="p-2 bg-warning"):
            text " Showing only the first 500."
    table(class="table"):
      thead:
        tr:
          for col in ["Position", "Ref", "Alt", "Position (AA)", "Ref AA",
                      "Alt AA", "ORF", "Kind", "Severity", "Description",
                      "Frequency"]:
            th:
              text col
      tbody:
        for v in data:
          tr(id= &"{v.refer}{v.position}{v.alt}"):
            td:
              text v.position
            td:
              text v.refer
            td:
              text v.alt
            td:
              text v.position_aa
            td:
              text v.ref_aa
            td:
              text v.alt_aa
            td:
              text v.orf_name
            td:
              if v.kind != $kUndefined:
                text v.kind
              else:
                text "-"
            td:
              text v.severity
            td:
              text v.description
            td:
              if v.frequency != "":
                text &"{(parseFloat(v.frequency) * 100):.2f}%"
              else:
                text "-"

proc tabContentNode(hap: Haplogroup, variants: seq[string],
                    resultData: StringTableRef, status: bool = false):
  VNode =
  var active: string = if status: "active" else: ""

  let reversions: seq[string] = filterIt(hap.pathway, it notin variants)

  let points: seq[Point] = selectHaplogroupLocCount(hap.name)

  buildHtml(tdiv(class= &"tab-pane {active}", id= &"hap_{hap.name}")):
    tdiv(class="div-center"):
      h2(class="text-center"):
        span(class="upper"):
          text "Haplogroup: "
        a(href= &"/phylogeny/{hap.name}"):
          text &"{hap.name}"
      mapDiv(hap.name, "", points)
    h2(class="mt-5"):
      text "Details for your sequence"
    # The name of the result
    if not isEmptyOrWhitespace(resultData["name"]):
      p:
        text &"""Named {resultData["name"]}"""
        # The group of the result
        # FIXME: This way doesn't show Group when no name is assigned
        if not isEmptyOrWhitespace(resultData["group"]):
          text " and assigned to "
          a(href= &"""/align/result/{resultData["uuid"]}/group/"""):
            text resultData["group"]
    elif not isEmptyOrWhitespace(resultData["group"]):
      text "Assigned to "
      a(href= &"""/align/result/{resultData["uuid"]}/group/"""):
        text resultData["group"]
    # The date of creation
    if not isEmptyOrWhitespace(resultData["created"]):
      try:
        let created = parse(resultData["created"], "yyyy-MM-dd'T'HH:mm:sszzz")
        tdiv(class="text-secondary font-italic"):
          text "Created on " & created.format("yyyy-MM-dd")
      except:
        discard

    if len(hap.pathway) > 0:
      tdiv(class="mt-2"):
        p:
          text "Your sequence contains the following variants, which make this " &
          "haplogroup the closest one."
        p:
          pathwayButtons(hap.pathway, variants)
    if len(reversions) > 0:
      tdiv:
        p:
          text "Except for this variant which seems to had reverted to the " &
          "reference sequence."
        p:
          pathwayButtons(hap.pathway, reversions, "danger")

proc tabAlignment(): VNode =
  buildHtml(a(class= "nav-item nav-link", href= &"#alignment-info",
              `data-toggle`="tab")):
    text "Alignment"

proc tabSuperspread(): VNode =
  buildHtml(a(class= "nav-item nav-link", href= &"#superspread-info",
              `data-toggle`="tab")):
    text "Superspreads"

iterator tabHaplogroups(haplogroups: seq[Haplogroup]): VNode =
  var tabStatus = "active"

  for haplogroup in haplogroups:
    yield buildHtml(a(class= &"nav-item nav-link {tabStatus}",
                      href= &"#hap_{haplogroup.name}",
                      `data-toggle`="tab")):
      text haplogroup.name
      tabStatus = ""

proc renderStats(stats: JsonNode): VNode =
  const refLength: int = 29903
  var coverage, nPercent: float
  var btnClass: string

  buildHtml(span):
    for qc_stat in ["length", "N"]:
      if stats.hasKey(qc_stat):
        button(class="btn btn-info"):
          text capitalizeAscii(qc_stat) & " "
          span(class="badge badge-light"):
            text $stats[qc_stat]
        if qc_stat == "length":
          coverage = (stats[qc_stat].getInt / refLength) * 100
          btnClass = if coverage < 95: "btn-warning" else: "btn-success"
          button(class= &"btn {btnClass}"):
            text "Coverage "
            span(class="badge badge-light"):
              text &"{coverage:5.2f}%"
        if qc_stat == "N":
          nPercent = stats[qc_stat].getInt / refLength * 100
          btnClass = if nPercent > 3: "btn-warning" else: "btn-success"
          button(class= &"btn {btnClass}"):
            text "N % "
            span(class="badge badge-light"):
              text &"{((stats[qc_stat].getInt / refLength) * 100):5.2f}%"

proc alignmentDiv(uuid: string, variants: seq[Variant]): VNode =
  let alignPath = getCurrentDir() / "public" / os.getEnv("alignDir")
  let stats = getResultStats(result_uuid = uuid)

  buildHtml(tdiv(id="alignment-info", class= &"tab-pane")):
    if fileExists(alignPath / $uuid & ".fasta"):
      tdiv(class="alert alert-info"):
        text "You requested the alignment. "
        a(href=os.getEnv("alignDir") / $uuid & ".fasta", download="align.fasta"):
          text "Here it is."
    if len(variants) < 500:
      tdiv(id="alignment-graphic", class="my-5"):
        ## This is a SVG that shows all the variants overlayed over the genome
        verbatim variantsOverGenomeSvg(variants.mapIt(parseInt(it.position)))
      # This is a table for the QC description of the sequence
      tdiv(id="quality-desc", class="my-5"):
        renderStats(stats)
    else:
      tdiv(class="alert alert-warning"):
        text "Too many variants to be represented."
      verbatim variantsOverGenomeSvg()

proc superspreadDiv(seevents: seq[StringTableRef]): VNode =
  ## Build the superspreadDiv for a given result Uuid
  ##
  buildHtml(tdiv(id="superspread-info", class= &"tab-pane")):
    h2:
      text "Superspreading events"

    for evt in seevents:
      let se_delay = (parse(evt["start_date"], "yyyy-MM-dd") -
                      parse(evt["first_date"], "yyyy-MM-dd"))
      let se_span = (parse(evt["end_date"], "yyyy-MM-dd") -
                     parse(evt["first_date"], "yyyy-MM-dd"))

      if fileExists(getCurrentDir() / "public" / static(evt["image"])):
        tdiv(class="my-2"):
          img(src=static(evt["image"]), class="rounded mx-auto d-block")
      p:
        text &"""On {evt["first_date"]} a sequence like this was first seen in
        {evt["country.name"]}. {se_delay.inDays} days later, on
        {evt["start_date"]} it started to super-spread through the community,
        and {evt["event_sequences"]} copies where collected along with
        {evt["near_sequences"]} closely related copies (one mismatch tops). The
        superspread stopped on {evt["end_date"]}, {se_span.inDays} days after it
        started, with {evt["total_sequences"]} sequences found in total."""
      p:
        text &"""The last sequence of the superspreading event was collected
        on {evt["last_date"]}."""

proc renderResultHeader*(rowData: StringTableRef): VNode =
  ## Renders the header of a Result Row table
  ##
  buildHtml(thead):
    tr:
      for col in rowData.keys:
        th(scope="col"):
          text title(col)

proc renderResultRow*(rowData: StringTableRef): VNode =
  ## Renders a Result Row, that is a strtabs:StringTable loaded with data
  ##
  buildHtml(tr):
    for k, v in rowData:
      td:
        case k
        of "uuid":
          a(href= &"/align/result/{v}/"):
            verbatim v.split("-")[0]
        of "created":
          verbatim parse(v, "yyyy-MM-dd'T'hh:mm:sszzz").format("yyyy-MM-dd")
        of "stats":
          if not v.isEmptyOrWhitespace:
            renderStats(parseJson(v))
        else:
          verbatim v

proc renderResultTable*(rowData: seq[StringTableRef]): VNode =
  ## Renders the full table for a Result Group.
  ##
  buildHtml(table(class="table")):
    renderResultHeader(rowData[0])
    tbody:
      for row in rowData:
        renderResultRow(row)

proc resultDetail*(uuid: string): string =
  getConn
  let variants = getResultVariants(result_uuid = uuid)
  let concernVariants = getResultConcernVariants(result_uuid=uuid)
  let superspreadEvents = getResultSuperspreaderEvents(result_uuid=uuid)

  let resultData = db.get("result", $uuid)

  let haplogroups = classify(variants)

  var isActive: bool = true

  let scripts: seq[string] = @["js/plotly-latest.min.js",
                               "js/haplotable.js"]
  let styles: seq[string] = @["css/tree.css"]

  let strVariants = setVariants(variants)

  let node = buildHtml(tdiv(class="div-center")):
    nav(class="nav nav-tabs"):
      for tabLink in tabHaplogroups(haplogroups):
        tabLink
      tabAlignment()
      if len(superspreadEvents) > 0:
        tabSuperspread()

    script:
      verbatim "window.onload=onLoad;"

    tdiv(class="tab-content"):
      for haplogroup in haplogroups:
        tabContentNode(haplogroup, strVariants, resultData, status=isActive)
        isActive = false
      alignmentDiv(uuid, variants)
      superspreadDiv(superspreadEvents)
      concernButtons(concernVariants)

    tdiv(id="raw-data", class="mt-3"):
      a(href= &"json"):
        text "Get serialized data"
      tdiv(id="variant-table"):
        tableVariant(variants)

  var context = {"active": "Align"}.newStringTable
  context["title"] = resultData["name"]
  baseHtml(node, styles, scripts, context=context)

proc resultGroup*(uuid: string): string =
  let samples = filterResultGroup(uuid)
  let node = buildHtml(tdiv):
    if len(samples) == 0:
      tdiv(class="alert alert-warning"):
        verbatim "This alignment wasn't included in any group."
        a(href= &"/align/result/{uuid}/"):
          text "Back to result details"
    else:
      h1:
        text samples[0].getOrDefault("group")
      tdiv:
          renderResultTable(samples)

  var groupId: string = ""
  if len(samples) > 0: groupId = samples[0].getOrDefault("group")
  var context = {"title": groupId}.newStringTable
  baseHtml(node, context=context)
