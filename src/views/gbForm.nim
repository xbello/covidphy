import strformat
import strtabs
import karax / [karaxdsl, vdom]

import base
import .. / forms
import .. / helpers / csrf

proc pageText: VNode =
  buildHtml(tdiv(class="my-3")):
    h3:
      text "Find the haplogroup for a "
      a(href="https://www.ncbi.nlm.nih.gov/nuccore", target="_new"):
        text "Genbank"
      text " sequence"
    tdiv:
      p:
        text &"""To save you a few clicks, enter a single Genbank ID and we
        will retrieve the sequence, perform the alignment locally and classify
        it."""

proc genbankForm*(csrfSecret: string, errors: seq[FormError] = @[]): string =
  let node = buildHtml(tdiv(class="col")):
    pageText()
    form(`method`="post"):
      tdiv:
        input(name="CSRFToken", type="hidden", value=maskToken(csrfSecret))
      tdiv(id="div_id_genbank", class="form-inline"):
        tdiv(class="input-group w-75 mx-auto"):
          input(name="genbankId", class="form-control", id="id_genbank",
                placeholder="E.g. MT450923 or MT444588.1")
          tdiv(class="input-group-append"):
            button(type="submit", name="submit", value="Submit",
                   class="btn btn-primary", id="submit-id-submit"):
              text "Search"

  markErrors(node, errors)

  var context = {"active": "Genbank Search"}.newStringTable
  baseHtml(node, context=context)
