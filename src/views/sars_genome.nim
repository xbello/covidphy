## Creates a SVG schema of the SARS-CoV-2 genome
##
import strformat
import nimsvg

type
  Orf = tuple[x, width: float, name: string]
const
  pxPerBase: float = 0.02838
  scaleZero: float = 3.345
  y2: float = 31.3
  yt: int = 11
  stride: int = 5000
  x1inc: float = 28.33

  # Odd Orfs:
  odds = @[(x: 11.14, width: 374.17, name: "ORF1a"),
           (x: 615.55, width: 107.94, name: "S"),
           (x: 748.43, width: 5.94, name: "E"),
           (x: 775.59, width: 4.75, name: "ORF6"),
           (x: 795.23, width: 9.85, name: "ORF8"),
           (x: 842.45, width: 2.79, name: "ORF10")]
  # Even Orfs:
  even = @[(x: 385.73, width: 229.01, name: "ORF1b"),
           (x: 724.25, width: 22.97, name: "ORF3a"),
           (x: 756.32, width: 18.45, name: "M"),
           (x: 781.04, width: 9.85, name: "ORF7a"),
           (x: 806.01, width: 35.23, name: "N")]

  height: float = 17.0
  oddY: float = 63.27
  oddCss: string = "st0a"
  evenY: float = 118.262
  evenCss: string = "st0b"

proc scaleBar: Nodes = buildSvg:
  var x1: float = 7

  g(transform="translate(-3.1549 -3.6504)"):
    line(class="st1", x1=x1, x2=857.5, y1=y2, y2=y2) # The main bar
    for i in 0 .. 30:
      let y1 = if i mod 5 == 0: 20 else: 25  # The height of the ticks
      line(class="st1", x1=x1, x2=x1, y1=y1, y2=y2)
      x1 += x1inc

  # The labels for the major ticks
  for i, d in [0.45, 130.51, 268.36, 410.08, 551.81, 693.54, 835.27]:
    text(class="st2 st3", x=d, y=yt):
      t $(i * stride)

proc drawOrf(r: Orf, y, height: float, css: string): Nodes = buildSvg:
  # Adjust the text position at the Orf center (aprox)
  let tx: float = (r.width / 2) + r.x - float(len(r.name)) * 4

  rect(class= &"st0 {css}", x=r.x, y=y, width=r.width, height=height)

  # A couple of Orfs has to put the text away from the previous Orf
  let tY = if r.name in @["ORF6", "M"]: (y + 30) else: (y - 5)
  text(class="st2 st3", x=tx, y=tY):
    t r.name

proc drawOrfs: Nodes = buildSvg:
  # Odd Orfs, goes in the upper part of the graph:
  for orf in odds:
    embed drawOrf(orf, y=oddY, height=height, css=oddCss)
  # Even Orfs, goes a little down:
  for orf in even:
    embed drawOrf(orf, y=evenY, height=height, css=evenCss)

proc genomeSchema(variants: seq[int]): Nodes = buildSvg:
  svg(xmlns="http://www.w3.org/2000/svg", version="1.1", viewBox="0 0 900 150"):
    style(`type`="text/css"):
      t ".st0{stroke:#000000;stroke-width:0.5;stroke-miterlimit:10;}"
      t ".st0a{fill:#1a5b82}"
      t ".st0b{fill:#17b2b6}"
      t ".st1{fill:none;stroke:#000000;stroke-miterlimit:10;}"
      t ".st2{font-family:Liberation Sans, Arial, Sans-serif;}"
      t ".st3{font-size:14px;}"
      t ".vars{fill-opacity:.56739;fill:#b61a17}"
    embed scaleBar()
    # Draw a vertical red bar per each variant
    for variant in variants:
      rect(x=(pxPerBase * float(variant)) + scaleZero, y=20, width=3,
           height=150, class="vars")

    embed drawOrfs()

proc variantsOverGenomeSvg*(variants: seq[int] = @[]): string =
  # Don't remove indent=1, it removes the header
  render(genomeSchema(variants), indent=1)

when isMainModule:
  echo variantsOverGenomeSvg(@[0, 10000, 24500])
