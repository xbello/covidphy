import json
import strtabs
import strutils
import sugar
import ../ models

proc resultJson*(uuid: string): JsonNode =
  ## Serialize a Result into a Json
  ##
  getConn
  let data = db.get("result", $uuid)
  let variants = getResultVariants(result_uuid = uuid)

  let variantsData = collect(newSeq):
    for k, v in variants:
      %* {"Ref": v.refer,
          "Position": parseInt(v.position),
          "Alt": v.alt,
          "RefAA": v.ref_aa,
          "PositionAA": if v.position_aa == "": 0 else: parseInt(v.position_aa),
          "AltAA": v.alt_aa,
          "Orf": v.orf_name,
          "Kind": v.kind,
          "Severity": v.severity,
          "Description": v.description,
          "Frequency": parseFloat(v.frequency)}

  %* {"id": data["uuid"],
      "stats": parseJson(data["stats"]),
      "created": data["created"],
      "variants": variantsData}
