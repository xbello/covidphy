import nimsvg


proc minus: Nodes = buildSvg:
  svg(id="i-minus", width="1em", height="1em", viewBox="0 0 24 24", fill="none",
      stroke="currentcolor", `stroke-linecap`="round", `stroke-linejoin`="round",
      `stroke-width`="2"):
    circle(cx="12", cy="12", r="10")
    line(x1="8", y1="12", x2="16", y2="12")

proc plus: Nodes  = buildSvg:
  svg(id="i-plus", width="1em", height="1em", viewBox="0 0 24 24", fill="none",
      stroke="currentcolor", `stroke-linecap`="round", `stroke-linejoin`="round",
      `stroke-width`="2"):
    circle(cx="12", cy="12", r="10")
    line(x1="12", y1="8", x2="12", y2="16")
    line(x1="8", y1="12", x2="16", y2="12")

proc link: Nodes = buildSvg:
  svg(id="i-link", width="1em", height="1em", viewBox="0 0 16 16", fill="currentColor"):
    path(d="M4.715 6.542L3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.001 1.001 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z")
    path(d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 0 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 0 0-4.243-4.243L6.586 4.672z")

proc icons: Nodes = buildSvg:
  svg(xmlns="http://www.w3.org/2000/svg", style="display: none"):
    embed plus()
    embed minus()
    embed link()

proc iconsSvg*: string =
  render(icons(), indent=1)  # Indent for what? To avoid the headers.
