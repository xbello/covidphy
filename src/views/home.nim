import karax / [karaxdsl, vdom]

import base

proc eachPage(id, title, subtitle, body, image: string): VNode =
  buildHtml(section(id= "section_" & $id)):
    tdiv(class="ctr"):
      tdiv(class="row"):
        tdiv(class="col-lg-6"):
          h2(class="text-uppercase gill"):
            text title
      tdiv(class="row"):
        tdiv(class="col-lg-6"):
          p:
            strong:
              text subtitle
      tdiv(class="row h-50"):
        tdiv(class="col-lg-4 col-8"):
          text body
        tdiv(class="col-lg-2 col-4"):
          img(src=static(image), class="img-fluid h-50")

proc homeView*(): string =
  let node = buildHtml(tdiv(class="section-holder")):
    eachPage("Align",
             "align",
             "Your sequence, classified",
             """We perform a quick alignment against the reference MN908947.3
             to find all the diffs and match the most likely haplogroup""",
             "img/homeAlign.svg")
    eachPage("Phylogeny",
             "phylogeny",
             "Carefully built",
             """We used a lot of non-Artificial Inteligence to build a
             phylogeny on top of which we worked the whole system. Browse the
             tree and find what regions of the world share haplogroups""",
             "img/homePhylogeny.svg")
    eachPage("gisaid",
             "Search Gisaid",
             "On the shoulders of giants",
             """We classified a lot of Gisaid sequences to build our phylogeny,
             and we offer a quick jump between a Gisaid ID and its haplogroup""",
             "img/homeGisaid.svg")
    eachPage("last",  # The last section must be id'ed "last"
             "Search Ncbi",
             "Just for your convenience",
             """Just have a list of codes? Just paste them on the search
             box, and we take care of the download, alignment and
             classification.""",
             "img/homeNcbi.svg")
    tdiv(class="row keepreading"):
      tdiv(class="col-12"):
        img(src=static("img/arrowDown.svg"), class="img-fluid")
      script:
        verbatim "window.onload=onLoad;"

  baseHtml(node, @["css/home.css"], @["js/home.js"])
