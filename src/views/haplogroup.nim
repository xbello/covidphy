import strformat
import strtabs
import uri
import karax / [karaxdsl, vdom]

import ../models
import plotly_maps

import base
import haplogroupTree


proc mapTable(points: seq[Point]): Vnode =
  ## Return a table with the points found
  ##
  buildHtml(table(class="table table-hover table-sm")):
    thead:
      tr:
        th(scope="row", class="text-left"):
          text "Country"
        for col in ["Number of sequences", "Sequences in total", "%"]:
          th:
            text col
    tbody:
      for point in points:
        tr:
          th(scope="row", class="text-left"):
            text point.name
          td:
            text $point.n
          td:
            text $point.total
          td:
            text &"{(point.n / point.total) * 100:.2f}%"

proc haplogroupDetail*(name: string, area: string = ""): string =
  let scripts: seq[string] = @["js/plotly-latest.min.js"]

  let mainHaplogroupUrl = &"/phylogeny/{name}/"
  let points: seq[Point] = selectHaplogroupLocCount(name, area)

  let node = buildHtml(tdiv(class="div-center")):
    h2(class="text-center haplogroup"):
      span(class="text-uppercase"):
        text &"Haplogroup "
      a(href=mainHaplogroupUrl):
        text name
      if area != "":
        text &" in {decodeUrl(area)}"
    mapDiv(name, decodeUrl(area), points)
    tdiv(class="mt-5"):
      mapTable(points)

  var context = {"active": "Phylogeny"}.newStringTable
  baseHtml(node, @[], scripts, context=context)

proc haplogroupList*(): string =
  let scripts: seq[string] = @["js/haplotree.js"]
  let styles: seq[string] = @["css/tree.css"]

  let node = buildHtml(tdiv):
    script:
      verbatim "window.onload=onLoad;"
    h1:
      text "Haplogroups"
    tdiv:
      p:
        text "The whole package of haplogroups available. Click on names to " &
        "view more details."
    renderTree()

  var context = {"active": "Haplogroups"}.newStringTable
  baseHtml(node, styles, scripts, context=context)
