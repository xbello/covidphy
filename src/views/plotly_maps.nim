## Docs at https://plotly.com/javascript/reference/scattergeo/
##         https://plotly.com/python/map-configuration/
import algorithm
import json
import math
import sequtils
import strformat
import strutils

import karax / [karaxdsl, vdom]

type
  ColorBar = object
    ## https://plotly.com/javascript/reference/heatmap/#heatmap-colorbar
    thickness: int
    title, ticksuffix, showticksuffix: string
  Line = object
    color: string
    width: float
  Marker = object
    color, size: seq[int]
    cmin, cmax: int
    reversescale: bool
    colorscale: string
    colorbar: ColorBar
    line: Line
  Data = object
    `type`, mode, hoverinfo: string
    text: seq[string]
    lon, lat: seq[float]
    showlegend: bool
    marker: Marker

  Projection = object
    `type`: string
  Axis = object
    range: seq[float]
  Geo = object
    ## https://plotly.com/javascript/reference/layout/geo/
    scope, fitbounds: string
    resolution: int
    showocean, showland, showsubunits: bool
    oceancolor, landcolor, countrycolor, bgcolor: string
    countrywidth: float
    projection: Projection
    lonaxis, lataxis: Axis
  Margin = object
    l, r, t, b: int
  Layout = object
    title: string
    autosize: bool
    width, height: int
    margin: Margin
    geo: Geo

  Point* = object
    # size is the diameter of the circle
    # color is the intensity of the color of the circle (0 to 100).
    name*, continent*: string
    lat*, lon*: float
    n*, total*: int

proc transposePoints(points: seq[Point]):
  tuple[name: seq[string], lat, lon: seq[float], n, size, color: seq[int]] =
  # If received some points, e.g. [(1.0, 1.0, 1, 1), (2.0, 2.0, 1, 1)]
  # Return the transposed data as tuples, e.g. ([1.0, 2.0], [1.0, 2.0], [1, 2], [1, 2])
  # It also calculates the values for the point size and color
  for p in points:
    add(result.name, p.name)
    add(result.n, p.n)
    add(result.lat, p.lat)
    add(result.lon, p.lon)
    add(result.size, clamp(log10(float(p.n)) * 23, 5.0, 100.0).toInt)
    add(result.color, toInt((p.n / p.total) * 100))

proc graphData*(points: seq[Point]): JsonNode =
  ## Create the Objects needed by Plotly to make a bubble map.
  ## Docs at https://plotly.com/javascript/reference/scattergeo/
  ##
  let dataT = transposePoints(points)
  let colorbar = ColorBar(
    title: "Frequency",
    ticksuffix: "%",
    thickness: 10,
    showticksuffix: "last")
  let line = Line(color: "black", width: 0.5)
  let marker = Marker(
    size: dataT.size,
    color: dataT.color,
    cmin: 0,
    cmax: clamp(max(dataT.color) + 10, 0, 100),
    colorscale: "YlGnBu",
    reversescale: true,
    colorbar: colorbar,
    line: line)

  var labels: seq[string]
  for item in zip(dataT.name, dataT.n):
    labels.add &"{item[0]}: {item[1]}"

  let data = Data(
    `type`: "scattergeo",
    mode: "markers",
    text: labels,
    lon: dataT.lon,
    lat: dataT.lat,
    hoverinfo: "text",
    marker: marker)

  result = % @[data]

proc graphLayout*(area: string = "world"): JsonNode =
  let projection = Projection(type: "miller")

  let geo = Geo(scope: toLowerAscii(area),
                fitbounds: "false",
                resolution: 50,  # This can be 50 or 110, being 50 higher res
                showocean: true,
                oceancolor: "ccd1d4",
                showsubunits: false,
                bgcolor: "b5b9bc",
                countrywidth: 1.0,
                countrycolor: "ccd1d4",
                projection: projection,
                showland: true,
                landcolor: "fdfdfd")
  let margin = Margin(l:0, r:0, t:10, b:10)
  %Layout(
    autosize: true,
    #width: 1200,
    margin: margin,
    height: 600,
    geo: geo)

proc removeAntarctica*(node: var JsonNode) =
  ## Re-plot the world map to zoom out the iced continent
  ##
  ## Limit the map to these axis
  node["geo"]["lonaxis"] = %Axis(range: @[-170.0, 195.0])
  node["geo"]["lataxis"] = %Axis(range: @[-55.0, 80.0])

proc zoomAustralia*(node: var JsonNode) =
  ## This is a fixing proc to solve that Plotly doesn't have Australia area
  ##
  # Limit the World to points in the set with "fitbounds = locations"
  node["geo"]["lonaxis"] = %Axis(range: @[95.0, 195.0])
  node["geo"]["lataxis"] = %Axis(range: @[-50.0, 0.0])

proc filterContinents*(pts: seq[Point]): seq[string] =
  ## Filter down a seq of Points to avoid a new DB trip.
  var continents: seq[string]
  for p in pts:
    continents.add p.continent
  sorted(deduplicate(continents))

proc mapDiv*(name, area: string, points: seq[Point]): VNode =
  ## Create the whole div that contains the map and the buttons below.
  ##
  var continents: seq[string]

  if len(points) == 0:
    return buildHtml(tdiv):
      text "No sequences found"

  var data = graphData(points = points)
  var layout = graphLayout(area)

  if area.isEmptyOrWhitespace():
    continents = filterContinents(points)
    removeAntarctica(layout)

  if area == "Australia":
    zoomAustralia(layout)

  buildHtml(tdiv(id= &"Map_{name}", class="mb-2 text-center")):
    for continent in continents:
      a(class="btn btn-outline-info", href= &"/phylogeny/{name}/{continent}/"):
        text continent
    script(`type`= "text/javascript"):
      verbatim &"var data = {data};"
      verbatim &"var layout = {layout};"
      verbatim &"Plotly.newPlot('Map_{name}', data, layout, {{displayModeBar: false, scrollZoom: false}});"

when isMainModule:
  echo graphData()
