import strtabs
import karax / [karaxdsl, vdom]

import base
import .. / forms
import .. / helpers / csrf

proc alignFormText(): VNode =
  result = buildHtml(tdiv):
    h3:
      text "Paste here your sequence. The input will be pre-processed"
    tdiv:
      ul:
        li:
          text "Lines starting with"
          verbatim " <code>&gt;</code> "
          text "will be silently dropped."
        li:
           text """Anything that is not a valid DNA nucleotide (ACG or T) will be ignored.
           That includes strange characters but also DNA indeterminations or gaps."""
        li:
          text "Sequence length will be capped at ~32 Kb (2"
          verbatim "<sup>15</sup> "
          text """bases). Reference is under 30 Kb, so you have up to 2 kb to
          be insertions or flanking sequence."""
    tdiv:
      text "The sequence will be interpreted linearly. If you try to use " &
        "intermingled formats such as clustal or phylip, your results will " &
        "be useless."
    tdiv:
      text "Here is a sample sequence to have a glimpse of a result page: "
      button(id="sampleSequence", class="btn btn-info"):
        text "(GB: MZ350188.1)"
    script:
      verbatim "window.onload=onLoad;"

proc alignForm*(csrfSecret: string, err: seq[FormError] = @[]): string =
  let scripts: seq[string] = @["js/sampleSequence.js"]
  let node = buildHtml(tdiv(class="col")):
    alignFormText()
    form(`method`="post", class="mt-2"):
      tdiv:
        input(name="CSRFToken", `type`="hidden", value=maskToken(csrfSecret))
      tdiv(id="sampleId", class="form-group"):
        label(`for`="id_sample"):
          text "ID"
        tdiv:
          input(`type`="text", name="id_sample", size="50", placeholder="Group###:SeqName")
          small(id="id_sample_help", class="form-text text-muted"):
            text "An optional identifier for the sequence. If you put a " &
              "colon \":\" in the ID, we will use the text before the " &
              "colon as a group tag."
      tdiv(id="div_id_sequence", class="form-group"):
        label(`for`="id_sequence"):
          text "Sequence"
        tdiv:
          textarea(name="sequence", cols="40", rows="10",
                   class="textarea form-control", id="id_sequence")
      tdiv:
        input(`type`="checkbox", name="saveAlignment", id="saveAlignment")
        label(`for`="saveAlignment"):
          text "Download the generated alignment"
      tdiv(class="form-group"):
        tdiv:
          input(`type`="submit", name="submit", value="Submit",
                class="btn btn-primary", id="submit-id-submit")
          input(`type`="reset", name="reset", value="Reset",
                class="btn btn-inverse", id="reset-id-reset")

  markErrors(node, err)

  var context = {"active": "Align"}.newStringTable
  baseHtml(node, @[], scripts, context=context)
