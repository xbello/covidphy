import karax / [karaxdsl, vdom]

import base

proc custom404*: string =
  let node = buildHtml(tdiv(class="div-center")):
    img(src=static("img/404.svg"))
  baseHtml(node)

proc custom500*: string =
  let node = buildHtml(html):
    head:
      style:
        verbatim """div{position:relative;} div > img {position:absolute;top:50%;left:50%;margin:100px 0 0 -150px;}"""
    body:
      tdiv:
        img(src=static("img/500.svg"))

  result = "<!DOCTYPE html>\n" & $node
