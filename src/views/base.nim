import os
import sequtils
import strformat
import strtabs
import strutils
import tables
import dotenv
import karax / [karaxdsl, vdom]


type
  MenuItem = ref object
    url, active: string

proc static*(url: string): string =
  initDotEnv().load()
  os.getEnv("staticDir") / url

proc navBar(active: string): VNode =
  let menuItems = newOrderedTable[string, MenuItem](8) # 4 > menuItems.len !!

  menuItems["Align"] = MenuItem(url: "/align/", active: "")
  menuItems["Phylogeny"] = MenuItem(url: "/phylogeny/", active: "")
  menuItems["Gisaid Search"] = MenuItem(url: "/gisaid/", active: "")
  menuItems["Genbank Search"] = MenuItem(url: "/gb/", active: "")
  menuItems["Superspreading"] = MenuItem(url: "/superspread/", active: "")
  menuItems["About"] = MenuItem(url: "/about/", active: "")

  if menuItems.hasKey(active):
    menuItems[active].active = "active"

  buildHtml(nav(class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark")):
    button(class="navbar-toggler", `type`="button", `data-toggle`="collapse",
           `data-target`=".navbar-collapse"):
      span(class="navbar-toggler-icon")
    a(class="navbar-brand", href="/"):
      img(src=static("img/logoSquareDark.png"), width="30", height="30",
          alt="CovidPhy")

    tdiv(class="collapse navbar-collapse"):
      tdiv(class="navbar-nav"):
        for k, v in menuItems.pairs:
          a(class= &"nav-item nav-link {v.active}", href=v.url):
            text k

proc baseHtml*(content: VNode, stylesheets, headScripts, bodyScripts:
  seq[string] = @[], context: StringTableRef = newStringTable()): string =
  let allStylesheets = concat(@["css/style.css", "css/bootstrap.min.css",
                                "css/animate.min.css"],
                              stylesheets)
  let allHeadScripts = headScripts
  let allBodyScripts = concat(@["js/jquery-3.5.1.slim.min.js",
                            "js/popper.min.js",
                            "js/bootstrap.min.js"], bodyScripts)
  let page = buildHtml(html(lang="en")):
    head:
      meta(charset="utf-8")
      meta(`http-equiv`="x-ua-compatible", content="ie=edge")
      meta(name="viewport", content="width=device-width, initial-scale=1")
      meta(name="author", content="X. Bello")

      link(rel="apple-touch-icon", sizes="180x180",
           href=static("/apple-touch-icon.png?v=1"))
      link(rel="icon", type="image/png", sizes="32x32",
           href=static("/favicon-32x32.png?v=1"))
      link(rel="icon", type="image/png", sizes="16x16",
           href=static("/favicon-16x16.png?v=1"))
      link(rel="manifest", href=static("/site.webmanifest?v=1"))
      link(rel="mask-icon", color="#5bbad5",
           href=static("/safari-pinned-tab.svg?v=1"))
      link(rel="shortcut icon", href=static("/favicon.ico?v=1"))
      meta(name="apple-mobile-web-app-title", content="CovidPhy")
      meta(name="application-name", content="CovidPhy")
      meta(name="msapplication-TileColor", content="#da532c")
      meta(name="theme-color", content="#ffffff")

      for stylesheet in allStylesheets:
        link(rel="stylesheet", href=static(stylesheet))
      for script in allHeadScripts:
        script(src=static(script), `type`="text/javascript")
      title:
        text "CovidPhy"
        if context.hasKey("title") and not context["title"].isEmptyOrWhitespace:
          text " - "
          text context.getOrDefault("title")
    body:
      header:
        navBar(context.getOrDefault("active", ""))
      main(class="container a__animated a__fadeIn", role="main"):
        tdiv(class="row"):
          content

      footer(class="footer bg-dark text-right"):
        tdiv(class="container"):
          span(class="text-muted"):
            text "Made with "
            a(href="https://nim-lang.org/"):
              img(src=static("img/logoNim.png"), height="25px")
            text " by "
            a(href="https://genpob.eu/home/who/1-xabier-bello/"):
              text "xbello"
            text " in 2020"

      for script in allBodyScripts:
        script(src=static(script), `type`="text/javascript")

  result = "<!DOCTYPE html>\n" & $page
