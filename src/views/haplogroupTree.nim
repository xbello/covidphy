import json
import os
import strformat

import karax / [karaxdsl, vdom, vstyles]
import icons  # to have iconsSvg

proc renderMutations(data: JsonNode): seq[VNode] =
  if data.hasKey("mutations"):
    for m in data["mutations"]:
      var s = buildHtml(span(class="mutation")):
        text getStr(m)
      result.add s

proc renderLeaf(data: JsonNode): VNode =
  buildHtml(span(class="haplogroup")):
    a(href= &"{getStr(data[\"name\"])}"):
      text getStr(data["name"])
    for node in renderMutations(data):
      node

proc renderBranch(data: JsonNode): VNode =
  buildHtml(span):
    tdiv(style = style(StyleAttr.display, "inline")):
      svg(class="i"):
        verbatim "<use xlink:href=\"#i-minus\" />"
    renderLeaf(data)

proc renderNodes(data: JsonNode): VNode =
  buildHtml(ul):
    for node in data:
      li(class="parent_li"):
        if node.hasKey("children"):
          renderBranch(node)
          renderNodes(node["children"])
        else:
          span:
            renderLeaf(node)

proc renderTree*(): VNode =
  let jsonNodes = parseFile(currentSourcePath().parentDir / "haplogroups.json")

  buildHtml(tdiv(class="tree")):
    verbatim iconsSvg()
    renderNodes(jsonNodes)
