## Views for the Superspreaders.
##
## Doesn't include the sequence detection as Superspreader Event, but only the
##  description of each event identified.
##
import httpcore
import os
import sequtils
import strformat
import strtabs
import strutils
import times

import karax / [karaxdsl, vdom]

import base
import ../ models


proc superspreaderDetail*(id: string): tuple[code: HttpCode, content: string] =
  let events = getSuperspreadWithCountries(@[id])
  if len(events) == 0: return (Http404, "")
  let event = events[0]

  let se_delay = (parse(event["start_date"], "yyyy-MM-dd") -
                  parse(event["first_date"], "yyyy-MM-dd"))
  let se_span = (parse(event["end_date"], "yyyy-MM-dd") -
                 parse(event["first_date"], "yyyy-MM-dd"))

  let content = buildHtml(tdiv):
    h2:
      text "Superspread event"
    if fileExists(getCurrentDir() / "public" / static(event["image"])):
      tdiv(class="my-2"):
        img(src=static(event["image"]), class="rounded mx-auto d-block")
    p:
      text &"""On {event["first_date"]} a superspreading event most likely
      occurred in {event["country.name"]}. {se_delay.inDays} days later
      ({event["start_date"]}) it started to super-spread through the community.
      A total of {event["event_sequences"]} copies of this genome (that
      represents the core of the outbreak), were reported in Gisaid, a proxy
      for primary transmissions, along with {event["near_sequences"]} genomes
      that are one-step mutation with respect to the core genome (a proxy for
      secondary transmissions). The signature for the likely superspreading
      event ends at about {event["end_date"]}, {se_span.inDays} days after it
      started, with {event["total_sequences"]} sequences found in total."""
    p:
      text &"""The last sequence of the superspreading event was collected
      on {event["last_date"]}."""

  var context = {"active": "Superspreader"}.newStringtable

  (Http200, baseHtml(content, context=context))

proc superspreadersList*: string =
  var superspreadEvents = getAllSuperspreadEvents(countries=true)
  let viewFields = ["event_sequences", "event_total_sequences", "start_date",
                    "end_date", "country.name", "haplogroup.name"]
  let colWidths = [1, 1, 2, 2, 2, 2, 2]
  var rowCount: int = 0
  var rowBgnd: string = ""

  let content = buildHtml(tdiv(class="container")):
    h1:
      text "Superspreading"
    p:
      text &"We have characterized {len(superspreadEvents)} superspreading events."

    # Main table
    tdiv(class="container text-right"):
      tdiv(class="row font-weight-bold bg-light border-bottom border-secondary py-2"):
        for (width, field) in zip(
          colWidths,
          ["", "Sequences", "Frequency", "Start date", "End date",
           "Region", "Haplogroup"]):
          tdiv(class= &"col-{width}"):
            text field
      for event in superspreadEvents:
        inc(rowCount)
        rowBgnd = if (rowCount mod 10 == 0): "bg-light" else: ""
        tdiv(class= &"row my-1 {rowBgnd}"):
          tdiv(class= &"col-{colWidths[0]} text-left"):
            a(href= &"{event[\"id\"]}/"):
              text "View Details"
          for (width, field) in zip(colWidths[1 .. ^1], viewFields):
            tdiv(class= &"col-{width}"):
              if field == "haplogroup.name":
                a(href= "/phylogeny/" & event[field]):
                  text event["haplogroup.name"]
              elif field == "event_total_sequences":
                let freq = parseInt(event["event_sequences"]) /
                           parseInt(event["event_total_sequences"])
                text $(&"{freq * 100:0.2f}%")
              else:
                text event[field]

  var context = {"active": "Superspreader"}.newStringtable

  baseHtml(content, context=context)
