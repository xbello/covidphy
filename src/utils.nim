template getErrors* =
  let errs = request.cookies.getOrDefault("formErrors")
  #Unset the cookie via expiring it
  setCookie("formErrors", "", daysForward(-1))
  var errors{.inject.}: seq[FormError]
  if errs != "":
    errors = parseJson(errs).to(seq[FormError])
