import strformat
# Package

version = strip(staticRead("src/VERSION"))
author = "Xabier Bello"
description = "A webpage/app to classify SARS-CoV-2 sequences."
license = "MIT"
srcDir = "src"
bin = @["covidphy", "covidphy_cli", "covidphy_gui", "migrate",
        "helpers/updateVariants"]

# Dependencies

requires "nim >= 1.4.0",
  "dotenv 1.1.1",
  "jester 0.5.0",
  "karax 1.1.3",
  "uuids 0.1.10",
  "nimsvg 0.2.0",
  "nigui 0.2.4",
  "https://gitlab.com/xbello/bio 0.2.6"

# Tasks
#
task test, "Full test suite":
  exec "testament p tests"
  #exec "testament html"
  #exec "firefox testresults.html"

task jvs, "Compile the JS scripts":
  for script in ["haplotable", "haplotree", "home", "sampleSequence"]:
    exec &"nim js -o:public/static/js/ -d:danger src/views/js/{script}.nim"
