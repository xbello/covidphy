import json
import unittest

import bio / sequences
import covidphy_cli

suite "Test CLI entrypoint":
  test "Clean sequence":
    # This cleaning differs from form sequence in what it doesn't check for
    #  ">" lines, and doesn't break at 2^15 bases (allows infinite bases)
    check cleanSequence(
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") == "ACGTacgt"
    check cleanSequence("12345!\"·$%") == ""

  test "Get a sequence from the stdin":
    let seq = getSequence(">Sample\nACGTACGT\nACGTACGT\n>Sample2\nACGTACGT")

    check seq.name == "Sample"
    check seq.record ?= Sequence(chain: "ACGTACGTACGTACGT", class: scDNA)

  test "Classify a sequence record":
    let sr = SequenceRecord(name: "Test",
                            record: Sequence(chain: "ACGTACGTACGTACGT",
                                             class: scDNA))
    check main(sr) == %* {
      "haplogroups":
        [{"name": "A",
          "pathway": []}],
      "variants":
        ["T2C","A18282G","C18283T","T18285C","G18292A","A18294G"],
      "length": 16}
