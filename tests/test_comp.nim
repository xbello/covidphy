import db_sqlite
import os
import strtabs

import models

proc `==`*(a, b: StringTableRef): bool =
  for k, v in a.pairs:
    if b.getOrDefault(k) != v: return false
  for k, v in b.pairs:
    if a.getOrDefault(k) != v: return false
  return true

proc loadFixtureFor*(db: DbConn, fixture: string) =
  ## Load a fixture straigth from the test dir
  ##
  db.loadFixture(currentSourcePath().parentDir() / "fixtures" /
                 fixture & ".sql")
