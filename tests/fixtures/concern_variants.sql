PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "concern_variant"
    ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "base" char(32) NOT NULL,
    "aa" char(32),
    UNIQUE(base));
INSERT INTO concern_variant VALUES(1,'C21575T','L5F');
INSERT INTO concern_variant VALUES(2,'G21600T','S13I');
END TRANSACTION;
