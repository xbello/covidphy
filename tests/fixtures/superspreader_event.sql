PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "superspreader_event"
      ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
       "start_date" date NOT NULL,
       "end_date" date NOT NULL,
       "first_date" date NOT NULL,
       "last_date" date NOT NULL,
       "event_sequences" integer unsigned,
       "event_total_sequences" integer unsigned,
       "n_sequences" integer unsigned,
       "total_sequences" integer unsigned,
       "near_sequences" integer unsigned,
       "image" char(255), "country_id" INTEGER REFERENCES "country" ("id")  DEFERRABLE INITIALLY DEFERRED, "haplogroup_id" INTEGER REFERENCES "haplogroup" ("id")  DEFERRABLE INITIALLY DEFERRED);
INSERT INTO superspreader_event VALUES(1,'2020-03-20','2020-03-25','2020-03-20','2020-04-15',44,850,63,1778,81,'img/superspreads/1.png',45,173);
INSERT INTO superspreader_event VALUES(2,'2020-06-22','2020-08-03','2020-05-13','2020-08-28',805,6513,929,9420,1498,'img/superspreads/2.png',45,31);
INSERT INTO superspreader_event VALUES(3,'2020-06-26','2020-06-30','2020-06-24','2020-07-22',30,257,122,3721,967,'img/superspreads/3.png',45,31);
INSERT INTO superspreader_event VALUES(4,'2020-06-28','2020-07-11','2020-06-09','2020-08-11',123,1240,196,7787,1122,'img/superspreads/4.png',45,31);
INSERT INTO superspreader_event VALUES(5,'2020-07-09','2020-07-15','2020-07-05','2020-08-06',46,993,76,6420,80,'img/superspreads/5.png',45,31);
INSERT INTO superspreader_event VALUES(6,'2020-07-10','2020-07-22','2020-07-06','2020-08-22',91,2497,147,8025,101,'img/superspreads/6.png',45,31);
INSERT INTO superspreader_event VALUES(7,'2020-07-13','2020-07-17','2020-07-07','2020-08-16',30,1032,73,7305,965,'img/superspreads/7.png',45,31);
INSERT INTO superspreader_event VALUES(8,'2020-07-14','2020-07-19','2020-07-04','2020-08-19',33,1250,75,7940,975,'img/superspreads/8.png',45,31);
INSERT INTO superspreader_event VALUES(9,'2020-07-15','2020-07-22','2020-06-29','2020-08-17',51,1775,125,8055,1036,'img/superspreads/9.png',45,31);
INSERT INTO superspreader_event VALUES(10,'2020-07-18','2020-08-17','2020-06-05','2020-09-01',418,5730,479,9563,960,'img/superspreads/10.png',45,31);
INSERT INTO superspreader_event VALUES(11,'2020-07-21','2020-07-31','2020-07-13','2020-08-24',84,2630,114,7397,84,'img/superspreads/11.png',45,31);
INSERT INTO superspreader_event VALUES(12,'2020-07-21','2020-07-28','2020-07-15','2020-08-26',52,1964,115,7093,601,'img/superspreads/12.png',45,31);
INSERT INTO superspreader_event VALUES(13,'2020-07-24','2020-07-28','2020-07-22','2020-08-30',31,1128,67,5744,513,'img/superspreads/13.png',45,31);
INSERT INTO superspreader_event VALUES(14,'2020-12-15','2020-12-21','2020-12-15','2021-01-01',43,100,62,223,46,'img/superspreads/14.png',45,2);
INSERT INTO superspreader_event VALUES(15,'2020-12-08','2020-12-14','2020-12-06','2020-12-21',85,163,92,302,32,'img/superspreads/15.png',10,34);
INSERT INTO superspreader_event VALUES(16,'2021-03-16','2021-03-21','2021-01-29','2021-04-08',36,773,57,8804,0,'img/superspreads/16.png',10,31);
INSERT INTO superspreader_event VALUES(17,'2020-03-20','2020-04-07','2020-03-13','2020-05-14',206,2412,248,5705,288,'img/superspreads/17.png',52,46);
INSERT INTO superspreader_event VALUES(18,'2020-03-28','2020-04-12','2020-03-12','2020-06-06',119,1740,197,6125,223,'img/superspreads/18.png',52,115);
INSERT INTO superspreader_event VALUES(19,'2020-04-10','2020-04-30','2020-04-05','2020-05-16',297,2172,342,3334,342,'img/superspreads/19.png',52,70);
INSERT INTO superspreader_event VALUES(20,'2020-04-11','2020-04-26','2020-04-04','2020-05-16',117,1782,143,3410,495,'img/superspreads/20.png',52,70);
INSERT INTO superspreader_event VALUES(21,'2020-04-13','2020-04-25','2020-03-27','2020-05-14',106,1505,158,4428,78,'img/superspreads/21.png',52,123);
INSERT INTO superspreader_event VALUES(22,'2020-04-14','2020-04-26','2020-04-12','2020-05-06',89,1436,107,2211,124,'img/superspreads/22.png',52,70);
INSERT INTO superspreader_event VALUES(23,'2020-08-09','2020-08-14','2020-07-29','2020-09-01',31,245,49,1367,65,'img/superspreads/23.png',52,70);
INSERT INTO superspreader_event VALUES(24,'2020-08-09','2020-08-17','2020-08-05','2020-11-15',59,326,236,5071,273,'img/superspreads/24.png',52,55);
INSERT INTO superspreader_event VALUES(25,'2020-08-25','2020-08-29','2020-08-11','2020-11-27',31,249,249,6208,175,'img/superspreads/25.png',52,35);
INSERT INTO superspreader_event VALUES(171,'2021-02-23','2021-02-26','2021-02-08','2021-04-28',35,732,44,3246,93,'img/superspreads/171.png',38,27);
COMMIT;
