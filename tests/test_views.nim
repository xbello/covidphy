import os
import db_sqlite
import json
import posix_utils
import strformat
import strtabs
import strutils
import times
import unittest

import karax / [karaxdsl, vdom]
import uuids

import forms
import models
import migrate
import views / [alignForm, home, resultDetail, resultJson, superspreader]

import test_comp

proc htmlToCompare(src: string): seq[string] =
  for line in src.splitLines:
    result.add strip(line)

suite "Test the align form":
  setup:
    let form = alignForm("csrftokenupto32charactersinlength")

  test "Expected fields are in the form":
    check "input name=\"CSRFToken\"" in form
    check "id=\"div_id_sequence\"" in form
    check "id=\"sampleId\"" in form

  test "A sample sequence is provided":
    let page = alignForm("csrftokenupto32charactersinlength")

    check "Here is a sample sequence to have a glimpse of a result" in page
    check """<button id="sampleSequence" class="btn btn-info">""" &
      """(GB: MZ350188.1)</button>""" in page

  test "Certain JS is loaded":
    let page = alignForm("csrftokenupto32charactersinlength")

    check "sampleSequence.js" in page

    check "button id=\"sampleSequence\"" in page

suite "Test the view of Result grouped by Group":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()

    putEnv("alignDir", "test_align")
  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "View is reachable":
    check resultGroup("GroupA").startsWith("<!DOCTYPE html>")

  test "Group Name and all Samples are included":
    var ids: seq[string]
    for x in 0 .. 4:
      ids.add($db.createResult())
      discard addResultName(ids[^1], &"GroupA:Sample{x}")

    let page = resultGroup(ids[^1])

    check "<title>CovidPhy - GroupA</title>" in page
    for id in ids:
      check id in page

    check &"<h1>GroupA</h1" in page

  test "Empty group renders empty page with warning":
    let id = $db.createResult()

    let page = resultGroup(id)

    check "<title>CovidPhy</title>" in page

    check "This alignment wasn't included in any group." in page
    # Provide a "back to alignment detail" link.
    check &"/align/result/{id}/" in page

suite "Test the Result Group rendering":
  setup:
    let initialData = {"uuid": "04c7d7a9-b3ed-4a7c-b6aa-1da107c314fe",
                       "stats": "{\"length\":29751}",
                       "created": "2021-03-30T18:07:05+02:00",
                       "name": "Name",
                       "group": "Group",
                       "status": "Done"}.newStringTable
    let expectedRow = """<tr>
        <td><a href="/align/result/04c7d7a9-b3ed-4a7c-b6aa-1da107c314fe/">04c7d7a9</a></td>
        <td><span>
          <button class="btn btn-info">Length <span class="badge badge-light">29751</span></button>
          <button class="btn btn-success">Coverage <span class="badge badge-light">99.49%</span></button>
        </span></td>
        <td>2021-03-30</td>
        <td>Group</td>
        <td>Name</td>
        <td>Done</td>
       </tr>"""

    let expectedHeader = """<thead><tr>
        <th scope="col">Uuid</th>
        <th scope="col">Stats</th>
        <th scope="col">Created</th>
        <th scope="col">Group</th>
        <th scope="col">Name</th>
        <th scope="col">Status</th>
       </tr></thead>"""

  test "Render of a single Result data":
    let render = buildHtml:
      renderResultRow(initialData)

    check htmlToCompare(expectedRow) == htmlToCompare($render)

  test "Render a Result Group table header":
    let render = buildHtml:
      renderResultHeader(initialData)

    check htmlToCompare(expectedHeader) == htmlToCompare($render)

  test "Render the Full Result Group Table":
    let render = buildHtml:
      renderResultTable(@[initialData])

    let expectedTable = &"""<table class="table">
     {expectedHeader}
     <tbody>{expectedRow}</tbody>
    </table>"""

    check htmlToCompare(expectedTable) == htmlToCompare($render)

suite "Test the view for Result Detail":
  ## This is an ongoing effort, as the view was developed without tests"
  ##
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()

    putEnv("alignDir", "test_align")
  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "Result Detail includes name, group and date details":
    let id = $db.createResult()
    discard addResultName(id, &"GroupA:Sample1")
    let resultData = db.get("result", id)

    var page = resultDetail(id)
    let today = now().format("yyyy-MM-dd")

    check "<title>CovidPhy - Sample1</title>" in page
    check "Named Sample1" in page
    check &"""and assigned to <a href="/align/result/{id}/group/">GroupA</a>""" in page
    check &"Created on {today}" in page

    # Remove the name, let only the group, and check group is still shown
    discard addResultName(id, &"GroupA:")

    page = resultDetail(id)
    check "Named Sample1" notin page
    check &"""Assigned to <a href="/align/result/{id}/group/">GroupA</a>""" in page

  test "Integration: perform an alignment":
    let sequence = readFile(currentSourcePath().parentDir() / "test_files" / "near.fas")
    db.exec(sql"""INSERT INTO concern_variant (base, aa)
            VALUES (?, ?)""", "C241T", "")
    let expected = @[{"base": "C241T", "id": "1", "aa": ""}.newStringTable]

    let (result_id, errors) = processSequence(sequence, "off", "")
    check len(errors) == 0

    check getResultConcernVariants(result_id) == expected

  test "Integration: Simulate a Sequence full alignment with Concern Variants":
    let sequence = readFile(currentSourcePath().parentDir() / "test_files" / "near.fas")

    db.exec(sql"""INSERT INTO concern_variant (base, aa)
            VALUES (?, ?)""", "C241T", "")
    let (result_id, errors) = processSequence(sequence, "off", "")

    var page = resultDetail(result_id)

    check &"""Your sequence contains the following variants of concern""" in page
    check """<button class="btn btn-warning" type="button" """ &
      """style="margin: 3px; ">C241T</button>""" in page

  test "Render Concern Variants button":
    let vc = @[{"base": "C241T", "id": "1", "aa": ""}.newStringTable]

    check splitLines($concernButtons(vc)) == @[
      "<div>",
      "  <p>Your sequence contains the following variants of concern:</p>",
      """  <span><button class="btn btn-warning" type="button" """ &
      """style="margin: 3px; ">C241T</button></span>""",
      "</div>"]

    check $concernButtons(@[]) == "<div></div>"

  test "Include the Superspreaders tab when Events found":
    db.loadFixtureFor("superspreader_event")
    db.loadFixtureFor("country")
    db.loadFixtureFor("haplogroup")
    let sequence = readFile(currentSourcePath().parentDir() / "test_files" /
                            "superspreader.fas")

    let (result_id, errors) = processSequence(sequence, "off", "")

    let page = resultDetail(result_id)

    check """<a class="nav-item nav-link" href="#superspread-info" """ &
      """data-toggle="tab">Superspreads</a>""" in page

    check "with 3246 sequences found in total" in page
    check "Turkey" in page

suite "Test the view for Superspreaders List and Detail":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()

    db.loadFixtureFor("superspreader_event")
    db.loadFixtureFor("country")
    db.loadFixtureFor("haplogroup")

    putEnv("alignDir", "test_align")
  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "Link for the Superspreader tab can be seen in all views":
     let page = homeView()

     check """<a class="nav-item nav-link " href="/superspread/">""" &
       """Superspreading</a>""" in page

  test "Superspreaders list view can be retrieved":
    let page = superspreadersList()

    check "Superspreading" in page

  test "A Superspreader resume can be seen in the top of the page":
    let page = superspreadersList()

    check "We have characterized 26 superspreading events" in page

  test "A Superspreader table can be seen in the page":
    let page = superspreadersList()

    for thead in ["Sequences", "Frequency", "Start date", "End date",
                  "Region", "Haplogroup"]:
      check thead in page

    for trow in ["View Details", "44", "5.18%", "2020-03-20", "2020-03-25",
                 "Australia", "/phylogeny/B3a1"]:
      check trow in page

  test "Superspreader Event detail page":
    let page = superspreaderDetail(id="1").content

    check "2020-03-20" in page
    check "2020-03-25" in page
    check "2020-04-15" in page
    check "44" in page
    check "1778" in page
    check "81" in page
    check "img/superspreads/1.png" in page

suite "Test the serialization of Results":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()
    db.loadFixtureFor("variant")

  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "Request a Json for a result":
    let id = $db.createResult()
    let stats = %* {"length": 29991}
    db.exec(sql"""UPDATE result SET stats = ? WHERE uuid = ?""", stats, id)
    let created = db.getValue(
      sql"""SELECT created FROM result WHERE uuid = ?""", id)

    for vId in 2 .. 5:
      db.exec(sql"""INSERT INTO results_variants (result_id, variant_id)
              VALUES (?, ?)""", id, vId)

    let page = resultJson(id)

    let expected = %*
      {"id": id,
       "stats": stats,
       "created": created,
       "variants":[
        {"Ref": "T", "Position": 10, "Alt": "W",
         "RefAA": "", "PositionAA": 0, "AltAA": "",
         "Orf": "", "Kind": "", "Severity": "",
         "Description": "Mutation", "Frequency": 4.22942268380366e-05},
        {"Ref": "C", "Position": 4331, "Alt": "T",
         "RefAA": "L", "PositionAA": 1356, "AltAA": "L",
         "Orf": "orf1a", "Kind": "Mutation", "Severity": "Low",
         "Description": "Synonymous", "Frequency": 0.00293279799443182},
        {"Ref": "G", "Position": 23383, "Alt": "T",
         "RefAA": "Q", "PositionAA": 607, "AltAA": "H",
         "Orf": "S", "Kind": "Mutation", "Severity": "Medium",
         "Description": "Missense", "Frequency": 6.83443322646082e-05},
        {"Ref": "G", "Position": 26792, "Alt": "T",
         "RefAA":"L", "PositionAA": 90, "AltAA": "F",
         "Orf": "M", "Kind": "Mutation", "Severity": "Medium",
         "Description": "Missense", "Frequency": 7.81078083024094e-05}]
       }

    check page == expected

  test "A link to the serialized response is available at Result Details":
    let id = $db.createResult()

    let page = resultDetail(id)

    assert "Get serialized data" in page
