import strformat
import strutils
import unittest

import forms
import data_clean


suite "More generic validators":
  test "Check the max length":
    check maxLength("12345", 5) == true
    check maxLength("12345", 4) == false

  test "Check the min length":
    check minLength("12345", 5) == true
    check minLength("12345", 6) == false

suite "Sequence form validations":
  test "Check the min length of the sequence":
    let cleanErrors: seq[FormError] = @[]
    let validSequence = "A".repeat(1 shl 15)

    let tooShort = FormError(
      level: elDanger,
      error: "Too short",
      field: "id_sequence",
      message: &"The sequence must be at least {1 shl 14} (2<sup>14</sup>) bases.")

    check isValid(validSequence) == cleanErrors
    check isValid("A") == @[tooShort]

suite "Gisaid Id form validations":
  test "Check the valid values":
    let noErrors: seq[FormError] = @[]
    check isGisaidValid("402119") == noErrors

  test "Check the empty value raises Error":
    let emptyError = FormError(
      error: "Empty",
      message: "The search box cannot be empty.",
      field: "id_gisaid",
      level: elDanger)
    check isGisaidValid("") == @[emptyError]

  test "Check some invalid per se integers":
    let errors = isGisaidValid("AAAA")
    check errors[0].error == "Not valid"

  test "Check some out of range values":
    # Values in the DB range from 402112 to 500000 (in the fixtures).
    var errors = isGisaidValid("-1")
    check errors[0].error == "Out of range"

    errors = isGisaidValid("0")
    check errors[0].error == "Out of range"

    errors = isGisaidValid("9999999")
    check errors[0].error == "Out of range"

suite "Sequence Id form validations":
  # They can be empty, both or individually
  # They cannot be more than 32 chars
  # Reduce them to [AZaz09]

  test "Check the lengths of group Id and sequence Id":
    let cleanErrors: seq[FormError] = @[]
    check idIsValid("") == cleanErrors
    check idIsValid("SampleA") == cleanErrors
    check idIsValid("Group1:SampleA") == cleanErrors

  test "Check failure for exceeding the length":
    let tooLongId = FormError(
      level: elDanger,
      error: "Too long",
      field: "id_sample",
      message: "Sequence Id is too long (> 32 chars)")

    let tooLongGroup = FormError(
      level: elDanger,
      error: "Too long",
      field: "id_sample",
      message: "Group Id is too long (> 32 chars)")

    let idLong = "X".repeat(33)
    let idGood = "X".repeat(32)
    check idIsValid(idLong) == @[tooLongId]
    check idIsValid(idLong & ":" & idGood) == @[tooLongGroup]
    check idIsValid(idLong & ":" & idLong) == @[tooLongGroup, tooLongId]

  test "Check failure for exceeding the length, weird chars":
    let idGood = "X".repeat(32) & "$!·$%&"
    let cleanErrors: seq[FormError] = @[]

    check idIsValid(idGood) == cleanErrors
    check idIsValid(idGood & ":" & idGood) == cleanErrors

  test "Check Id String cleansing":
    let original = "X".repeat(32) & "$!·$%&"

    check cleanId(original) == (name: "X".repeat(32), group: "")
    check cleanId("XXXX:YYYY") == (name: "YYYY", group: "XXXX")
    check cleanId("") == (name: "", group: "")

    # Some corner cases, cause people
    check cleanId(":") == (name: "", group: "")
    check cleanId("GGGG:") == (name: "", group: "GGGG")
    check cleanId(":NNNN") == (name: "NNNN", group: "")
    check cleanId(":NNNN:MMMM") == (name: "NNNN:MMMM", group: "")
