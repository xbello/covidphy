import strutils
import unittest

import helpers / csrf


suite "CSRF cookie + form protection":
  test "Secret string creation":
    let secret = newSecret()

    check len(secret) == DefaultSecretSize
    for c in secret:
      check c in IdentChars

  test "Token masking":
    let secret = newSecret()

    let maskedToken = maskToken(secret)

    check len(maskedToken) == DefaultTokenSize
    check secret notin maskedToken
    for c in secret:
      check c in IdentChars

  test "Token recovery":
    let secret = newSecret()

    let maskedToken = maskToken(secret)

    check unmaskToken(maskedToken) == secret
