import os
import sequtils
import strutils
import tables
import unittest

import helpers / [aligner, pathways, superspreaders]


suite "Test Mafft Aligner":
  setUp:
    let refer: string = "ACGTACGTACGTACGT"
    let empty: seq[string] = @[]

  test "Diff calculator, normal string":
    check diffs(@[refer, "ZCGZACGTACGTACGz"]) == @["A1Z", "T4Z", "T16Z"]

  test "Diff only reports where sequence exists":
    check diffs(@[refer, ""]) == empty
    check diffs(@[refer, "A--------------T"]) == empty

  test "Create a joined string pre-alignment":
    let query: string = "CCGCACGCGTACGC"

    let tmpAlign = split(preAlign(query), "\n")

    check tmpAlign[0] == ">MN908947.3"
    check len(tmpAlign[1]) == 29903  # Bases in reference
    check tmpAlign[2] == ">Query"
    check tmpAlign[3] == toLowerAscii(query)

  test "Align a string":
    let alignData = join(@[">Ref",
                           toLowerAscii(refer),
                           ">Target",
                           toLowerAscii("cCGCACGCGTACGc")],
                           "\n")

    check align(alignData) == @["acgtacgtacgtacgt", "ccgcacg--cgtacgc"]

  test "Get the diffs of a string":
    ## Mafft aligns our sequence to an odd place, but this gets the variants
    var al = diffString("aagaatgtagtttacaggcatgtactcaacatcaaccatatgtagt")
    check diffs(al) == @["T27963G"]
    al = diffString("")
    check diffs(al) == empty

  test "Get the diffs of a string when a insertion is present":
    # BUG: Positions after an insertion are shifted
    # Checking sequence is the start of the Reference with changes in uppercase
    let al = diffString(
      "attaaaggtttataccttcccaggtaacaaaccaaccaactttcgatctcttgtagatctgttct" &
      "ctaaacgaactttaaaatctgtgtggctgtcactcggctgcatgcttagtgcactcacgcagtatTGT" &
      "aattaataactaattactgtcgttgacaggacacgagtaactcgtctatcttctgcaggctgctt" &
      "acgCtttcgtccgtg")
    check diffs(al) == @["-129T", "-129G", "G199C"]

  test "Save the alignment as a given file":
    let output: string = currentSourcePath.parentDir() / "test_files" /
      "test_align.fas"
    let al = diffString("aagaatgtagtttacaggcatgtactcaacatcaaccatatgtagt",
                        output)
    check diffs(al) == @["T27963G"]

    check fileExists(output)
    check tryRemoveFile(output)

suite "Test the finder of variants":
  ## At first we searched only for single point variants (no indels, no MNPs).
  ## With the inclussion of Variants of Concern we need to find some of both
  ## in the alignments.
  test "The variant as string can be parsed in its parts":
    check parseVariant("A5T") == (r: "A", a: "T", p: 5)
    check parseVariant("AAA5---") == (r: "AAA", a: "---", p: 5)

  test "The building of the table of variants":
    var t = {1: @[(r: "C", a: "-"), (r: "C", a: "A")],
         6: @[(r: "G", a: "C")],
         8: @[(r: "A", a: "-")]}.toTable

    check tableVariants(@["G6C", "C1-", "A8-", "C1A"]) == t

  test "Find if a given variant is in the alignment":
    # Variant exists
    check findDiff(@["AAAAAA", "TTTTTT"], "A5T") == true
    # Variant don't exist
    check findDiff(@["AAAAAA", "TTTTTT"], "A5C") == false
    # MNP exists
    check findDiff(@["AAACAA", "TTCTTT"], "AC3CT") == true
    # MNP doesn't exist
    check findDiff(@["AAACAA", "TTCTTT"], "AC4CT") == false
    # Deletions also found
    check findDiff(@["AAACAA", "TT--TT"], "AC3--") == true
    # Deletion doesn't exist
    check findDiff(@["AAACAA", "TT--TT"], "AC4--") == false

  test "Find if a sequence of variants are in the alignment":
    # Both variants exists
    check findDiffs(@["AAAAAA", "TTTTTT"], @["A2T", "A5T"]) == @["A2T", "A5T"]
    # Only one variant exists
    check findDiffs(@["ACAAAA", "TTTTTT"], @["A2T", "A5T"]) == @["A5T"]
    # Check for deletions
    check findDiffs(@["ACAAAA", "TTTT-T"], @["A2-", "A5-"]) == @["A5-"]

  test "Find if a sequence in lowercase contains the variants":
    # Both variants exists
    check findDiffs(@["aaaaaa", "tttttt"], @["A2T", "A5T"]) == @["A2T", "A5T"]
    # Only one variant exists
    check findDiffs(@["acaaaa", "tttttt"], @["A2T", "A5T"]) == @["A5T"]

suite "Test pathways":
  setup:
    var haploA: Haplogroup = Haplogroup(name: "A", pathway: @[])

  test "Get Pathway from chunk of decimals":
    let empty: seq[string] = @[]
    check getPathway(phylogeny, "A") == empty
    check getPathway(phylogeny, "A2") == @["C241T", "C3037T", "A23403G"]
    check getPathway(phylogeny, "B") == @["C8782T", "T28144C"]

  test "Classify the main Haplogroup, without reversions":
    var variants = @[""]
    check classifyInHaplogroups(phylogeny, variants) == @[haploA]

    variants = @["C14805T", "G11083T", "G26144T", "C33333G"]
    var haploA1a = Haplogroup(name: "A1a",
                              pathway: @["G11083T","C14805T", "G26144T"])
    check classifyInHaplogroups(phylogeny, variants) == @[haploA1a]

  test "Classify the reversion Haplogroup, allowing 1 reversion":
    let expected: Haplogroup = Haplogroup(
      name: "A1a3a1",
      pathway: @["A2480G", "C2558T", "T6971C", "G6975T", "G6977A", "C9170T",
                 "G11083T", "C14805T", "G26144T"])
    let variants = @["A2480G", "C2558T", "T6971C", "G6975T", "G6977A",
                     "C9170T", "G11083T", "C14805T"] # reverted "G26144T"
    check classifyInHaplogroups(phylogeny, variants) == @[expected]

  test "Classify with double matching":
    # TODO
    check 1 == 1

  test "Classify a sequence without known Variants as A":
    let variants = @["G111111T", "G22222T", "C33333G"]
    check classifyInHaplogroups(phylogeny, variants) == @[haploA]

  test "The building of signatures":
    # Variants 0, 2, 63, 64, 65, 127, 128 and last one
    let variants = @["A187G", "C313T", "G11083T", "G11417T", "A11430G",
                     "G24197T", "G24368T", "G29751C"]
    let signature = @[
      "0b1010000000000000000000000000000000000000000000000000000000000001",
      "0b1100000000000000000000000000000000000000000000000000000000000001",
      "0b1000000000000000000000000000000000000000000000000000000000000100"]
    check buildSignature(phylogeny, @[""]) == @[0'i64, 0, 0]
    check buildSignature(phylogeny, variants) == mapIt(signature,
                                                       int64(it.parseBinInt()))

suite "Test superspreaders":
  ## This suite is very similar to pathways, as it uses the same logic and
  ## functions
  setup:
    let empty: seq[int64] = @[]
    let emptyStr: seq[string] = @[]
    let emptyEvt: seq[Haplogroup] = @[]

  test "Get Event Signature from the Id of the event":
    check getEventSignature(superspreadersPhy, "1") == @[
      0'i64, 0, 524288, 0, 16384, 576460752303423488, 0, 2251799813685248, 0, 0,
      0, 0, 128, 0, 17180000257, 2251799813685248]
    check getEventSignature(superspreadersPhy, "") == empty

  test "Get Event Variants from the Id of the event":
    let expected = @["C241T", "A1163T", "C3037T", "T7540C", "C14408T",
                     "G16647T", "C18555T", "G22992A", "G23401A", "A23403G",
                     "GGG28881AAC"]
    check getEventPathway(superspreadersPhy, "2") == expected
    check getEventPathway(superspreadersPhy, "X") == emptyStr

  test "Compute the number of diffs between signatures":
    let sign1 = getEventSignature(superspreadersPhy, "4")
    let sign2 = getEventSignature(superspreadersPhy, "5")

    check diffs(sign1, sign2) == 3
    check diffs(sign1, sign1) == 0

  test "Get the superspreader event of a sequence of variants":
    let vars = @["C241T", "C913T", "C3037T", "C3267T", "C5388A", "C5986T",
                 "T6954C", "TCTGGTTTT11288---------", "G11521T", "C12970T",
                 "C14408T", "C14676T", "C15279T", "T16176C", "TACATG21765------",
                 "TTA21991---", "A23063T", "C23271A", "A23403G", "C23604A",
                 "C23709T", "T24506G", "G24914C", "C27972T", "G28048T",
                 "A28111G", "A28271-", "GAT28280CTA", "GGG28881AAC"]
    check getEvents(superspreadersPhy, vars) == @["262"]

    let varsUndef = @["C241T", "A1163T", "C3037T", "T7540C", "C14408T",
                      "G15594T", "C22480T", "G16647T", "C18555T", "G22992A",
                      "G23401A", "A23403G", "GGG28881AAC"]
    check getEvents(superspreadersPhy, varsUndef) == @["4", "313", "9", "318"]

    check getEvents(superspreadersPhy, emptyStr) == emptyStr

    let varsNonExistent = @["G111111T", "G22222T", "C33333G"]
    check getEvents(superspreadersPhy, varsNonExistent) == emptyStr

  test "Get the superspreader event details of a sequence of variants":
    let vars = @["C241T", "C913T", "C3037T", "C3267T", "C5388A", "C5986T",
                 "T6954C", "TCTGGTTTT11288---------", "G11521T", "C12970T",
                 "C14408T", "C14676T", "C15279T", "T16176C", "TACATG21765------",
                 "TTA21991---", "A23063T", "C23271A", "A23403G", "C23604A",
                 "C23709T", "T24506G", "G24914C", "C27972T", "G28048T",
                 "A28111G", "A28271-", "GAT28280CTA", "GGG28881AAC"]
    let event2: Haplogroup = Haplogroup(name: "262", pathway: vars)

    check getEventsDetails(superspreadersPhy, vars) == @[event2]

    check getEventsDetails(superspreadersPhy, @[""]) == emptyEvt
