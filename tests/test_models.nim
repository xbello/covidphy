import db_sqlite
import json
import os
import posix_utils
import sequtils
import sets
import strtabs
import strutils
from sugar import collect
import times
import unittest

import uuids
import bio / fasta

import helpers / [aligner, superspreaders]
import models
import migrate
import views / plotly_maps

import test_comp

suite "Test model basics, no initialization needed":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
  teardown:
    removeDir(dbPath.splitPath().head)

  test "Get the DB from env":
    getConn
    let tables = db.getValue(
      sql"SELECT name FROM sqlite_master WHERE type='table'")
    check tables == ""

  test "Initialization of the DB":
    getConn
    let expected = @["results_variants", "result", "variant", "country",
                     "haplogroup", "countries_haplogroups", "gisaid",
                     "sqlite_sequence", "concern_variant",
                     "results_concernvariants", "superspreader_event",
                     "results_seevents"]
    db.migrate()
    for tName in db.instantRows(
      sql"SELECT name FROM sqlite_master WHERE type='table'"):
      check tName[0] in expected

  test "Add fixtures to the DB":
    getConn
    db.migrate()
    check len(db.getAllRows(sql"SELECT * FROM country")) == 0

    db.loadFixtureFor("country")

    check len(db.getAllRows(sql"SELECT * FROM country")) > 0

suite "Test db plays":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()
  teardown:
    removeDir(dbPath.splitPath().head)

  test "Get a value using the PK from a table":
    check db.get("country", "1")["name"] == ""

    db.loadFixtureFor("country")
    check db.get("country", "1")["name"] == "Asia"

    db.loadFixtureFor("haplogroup")

    check db.get("haplogroup", "23")["name"] == "A1/A3"

  test "Insert new Variant in the DB":
    check db.insertNewVariant('A', 'T', 12345) == 1
    let expected = {
      "id": "1", "alt": "T", "ref": "A", "ref_aa": "S", "alt_aa": "I",
      "position": "12345", "position_aa": "4027", "orf_name": "orf1a",
      "kind": "Mutation", "severity": "Medium", "description": "Missense",
      "frequency": ""}.newStringTable()

    check db.get("variant", "1") == expected

    # No double insertion
    check db.insertNewVariant('A', 'T', 12345) == 1

  test "Create a new Result":
    let resultUuid: UUID = db.createResult()

    let resultRow = db.get("result", $resultUuid)

    check resultRow["uuid"] == $resultUuid
    check resultRow["status"] == "Pending"
    # The result was created less than one second ago.
    check (getTime() - 1.seconds) < parseTime(resultRow["created"],
                                              "yyyy-MM-dd\'T\'HH:mm:sszzz", utc())

  test "Add Variant to Result":
    let pk1 = db.insertNewVariant('A', 'T', 12345)
    let pk2 = db.insertNewVariant('A', 'G', 12345)

    let resultUuid: UUID = db.createResult()

    check db.getAllRows(sql"SELECT * FROM results_variants").len == 0
    db.addVariantsToResult($resultUuid, @[pk1, pk2])

    check db.getAllRows(sql"SELECT * FROM results_variants").len == 2
    # Check we can get the Result from a given Variant
    check db.getValue(sql"""
      SELECT uuid FROM result
      JOIN results_variants ON result_id==result.uuid
      WHERE variant_id=?""", pk1) == $resultUuid

  test "Get all Variants of a Result":
    let pk1 = db.insertNewVariant('A', 'T', 12345)
    let pk2 = db.insertNewVariant('A', 'G', 12345)

    let v1: Variant = (id: $pk1, alt: "T", refer: "A", ref_aa: "S", alt_aa: "I",
                       position: "12345", position_aa: "4027", orf_name: "orf1a",
                       kind: "Mutation", severity: "Medium",
                       description: "Missense", frequency: "")
    let v2: Variant = (id: $pk2, alt: "G", refer: "A", ref_aa: "S", alt_aa: "S",
                       position: "12345", position_aa: "4027", orf_name: "orf1a",
                       kind: "Undefined", severity: "Low",
                       description: "Synonymous", frequency: "")

    let resultUuid: UUID = db.createResult()
    db.addVariantsToResult($resultUuid, @[pk1, pk2])

    check getResultVariants($resultUuid) == @[v1, v2]

  test "Update an existent Variant":
    let pk1 = db.insertNewVariant('A', 'T', 12345)

    check db.updateVariant(pk1, {"frequency": "0.75"}) == 1

    check db.getValue(sql"""
      SELECT frequency FROM variant
      WHERE ref=? AND alt=? AND position = ?""", 'A', 'T', 12345) == "0.75"

    check db.updateVariant(pk1, {"severity": $sHigh}) == 1
    check db.getValue(sql"""
      SELECT severity FROM variant
      WHERE ref=? AND alt=? AND position = ?""", 'A', 'T', 12345) == $sHigh

    check db.updateVariant(pk1, {"severity": $sLow,
                                 "frequency": "0.95"}) == 1
    check db.getRow(sql"""
      SELECT frequency, severity FROM variant WHERE id=?""", pk1) ==
      @["0.95", $sLow]

  test "Check if a Variant exists":
    check db.variantId('A', 'T', 12345) == 0

    let pk1 = db.insertNewVariant('A', 'T', 12345)

    check db.variantId('A', 'T', 12345) == pk1

suite "Test some complex queries":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()
    db.loadFixtureFor("country")
    db.loadFixtureFor("haplogroup")
    db.loadFixtureFor("countries_haplogroups")

  teardown:
    removeDir(dbPath.splitPath().head)

  test "Get the sum of Haplogroups for all countries in all continents":
    let countries = selectHaplogroup("A")
    check len(countries) == 63

  test "Get the sum of Haplogroups per continent":
    let europe = selectHaplogroup("A", "Europe")
    check len(europe) == 26

    let asia = selectHaplogroup("B3a", "Asia")
    check len(asia) == 3

  test "Get the sum of Haplogroups per continent":
    let world = selectWorldHaplogroup("B3a")

    # The continents match the expected
    check toHashSet(world.mapIt(it[0])) == toHashSet(
      @["Asia", "Australia", "Europe", "North America", "South America"])

    # The sums match the expected
    check toHashSet(world.mapIt(parseInt(it[3]))) ==
      toHashSet(@[21, 6, 510, 166, 13])

  test "Filter the continents where a Haplogroup can be found":
    var query = selectHaplogroupLocCount("B3a", "")

    check filterContinents(query) ==
      @["Asia", "Australia", "Europe", "North America", "South America"]

suite "Track migrations":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()
  teardown:
    removeDir(dbPath.splitPath().head)

  test "A new field that self-references is added":
    let parentId = db.insertId(sql"""INSERT INTO country
      (id, name, latitude, longitude, continent)
      VALUES (?, ?, ?, ?, NULL)""",
      1, "Continent Name", 1.234, 2.345)
    let childId = db.insertId(sql"""INSERT INTO country
      (id, name, latitude, longitude, continent)
      VALUES (?, ?, ?, ?, ?)""",
      2, "Country Name", 3.456, 4.567, parentId)

    let continents = db.getAllRows(sql"""SELECT id FROM country
      WHERE continent IS NULL""")
    let countries = db.getAllRows(sql"""SELECT id FROM country
      WHERE continent = ?""", parentId)

    check len(continents) == 1
    check continents[0][0] == $parentId

    check len(countries) == 1
    check countries[0][0] == $childId

  test "Load the country fixture and data":
    db.loadFixtureFor("country")

    let continents = db.getAllRows(sql"""SELECT id FROM country
      WHERE continent IS NULL""")

    check len(continents) == 7

  test "Select countries by continent, using two queries":
    db.loadFixtureFor("country")

    let europeId = db.getValue(sql"""SELECT id FROM country
      WHERE name = ?""", "Europe")
    let euroCountries = db.getAllRows(sql"""SELECT id FROM country
      WHERE continent = ?""", europeId)

    check europeId == "5"  # Values from the fixture
    check len(euroCountries) == 41

  test "Select countries by continent, using a subquery":
    db.loadFixtureFor("country")

    let euroCountries = db.getAllRows(sql"""SELECT id
      FROM country
      WHERE continent = (
        SELECT id FROM country WHERE name = ?)""",
      "Europe")

    check len(euroCountries) == 41

suite "Alignment":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()

    putEnv("alignDir", "test_align")
  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "The mafft alignment is saved as a file":

    let uuid = alignMafft("AAAAA", saveFile=true)
    let alignPath = getCurrentDir() / "public" / os.getEnv("alignDir")

    check dirExists(alignPath)
    check fileExists(alignPath / $uuid & ".fasta")

    let fileSeqs = load(alignPath / $uuid & ".fasta")

    check len(fileSeqs) == 2
    check fileSeqs[0].name == "Reference"
    check fileSeqs[1].name == "Query"

    check len(fileSeqs[0]) == len(fileSeqs[1])

  test "Store the alignment statistics":
    let uuid = alignMafft("AAAAA", saveFile=true)

    let stats = %* {"N": 1, "length": 5}

    check updateStats(uuid, stats) == 1

    check len(db.get("result", uuid)) == 6  # Six fields in the model.
    check parseJson(db.get("result", uuid)["stats"]) == stats

  test "Check stats created from a string":
    check sequenceStats("ACGTANNNNCGTACRWGTACGTNNNNN") ==
      parseJson("""{"N": 9, "W": 1, "R": 1, "length": 16}""")

    check sequenceStats("ACGTANNNNCGTACRWGTA----CGTNNNNN") ==
      parseJson("""{"N": 9, "W": 1, "R": 1, "length": 16}""")

  test "Check stats from a sequence raw from the form":
    check sequenceStats(">My Sequence\nACGTANNNNCGTA\nCRWGTACGTNNNNN") ==
      parseJson("""{"N": 9, "W": 1, "R": 1, "length": 16}""")

  test "Retrieve stats from the DB":
    let uuid = alignMafft("AAAAA", saveFile=true)

    let stats = %* {"N": 1, "length": 5}

    check updateStats(uuid, stats) == 1

    check getResultStats(uuid) == stats

  test "Retrieve empty stats from the DB":
    let uuid = alignMafft("AAAAA", saveFile=true)

    # Empty rows in the DB returns an empty Json.
    check getResultStats(uuid) == %* {}

suite "Grouping and Naming of alignments":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()

    putEnv("alignDir", "test_align")
  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "Add a valid group and name to the result":
    let uuid = alignMafft("AAAAA")
    check addResultName(uuid, "GroupA:Sequence A")
    var expected  = {
      "uuid": uuid, "status": "Done", "stats": "", "name": "SequenceA",
      "group": "GroupA"}.newStringTable()

    var obtained = db.get("result", uuid)
    obtained.del("created")

    check obtained == expected

    check addResultName(uuid, "")
    expected["name"] = ""
    expected["group"] = ""
    obtained = db.get("result", uuid)
    obtained.del("created")
    check obtained == expected

  test "Get a group of Results together":
    let oddName = "GroupOdd:SampleOdd"
    let evenName = "GroupEven:SampleEven"
    var id: string
    var ids: seq[string]

    for r in 0 .. 7:
      id = $db.createResult()
      ids.add id
      case r mod 2
      of 0:
        check addResultName(id, evenName)
      else:
        check addResultName(id, oddName)
    discard db.createResult()  # Create an empty Result to meddle the filters

    let evens: seq[StringTableRef] = filterResultGroup(ids[0])
    let odds: seq[StringTableRef] = filterResultGroup(ids[1])
    check len(odds) == 4
    check len(evens) == 4

    for row in odds:
      check row["name"] == "SampleOdd"
      check row["group"] == "GroupOdd"
    for row in evens:
      check row["name"] == "SampleEven"
      check row["group"] == "GroupEven"

  test "Empty group query returns always nothing":
    # To protect the view to retrieve every result without group
    let id = $db.createResult()
    discard addResultName(id, "")

    let ungrouped: seq[StringTableRef] = filterResultGroup("")

    check len(ungrouped) == 0

suite "Concern Variants":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()

    db.loadFixtureFor("concern_variants")

    putEnv("alignDir", "test_align")
  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "Can get the variants of concern":
    let expected  = {
      "id": "1", "base": "C21575T", "aa": "L5F"}.newStringTable
    check getConcernVariant("C21575T") == expected

  test "If variant doesn't exist, return empty StringTable":
    var expected = {"": ""}.newStringTable
    check getConcernVariant("C2T") == expected

  test "Get all the concern variants from the DB":
    let expected = @[
      {"id": "1", "base": "C21575T", "aa": "L5F"}.newStringTable,
      {"id": "2", "base": "G21600T", "aa": "S13I"}.newStringTable]
    check getAllConcernVariants() == expected

  test "Save the Concern Variants for a Result":
    let expected = @[
      {"id": "1", "base": "C21575T", "aa": "L5F"}.newStringTable,
      {"id": "2", "base": "G21600T", "aa": "S13I"}.newStringTable]
    let resId = db.createResult()

    check insertResultConcernVariants($resId, @["1"])

    check getResultConcernVariants($resId)[0] == expected[0]

  test "Save multiple Concern Variants for a Result":
    let expected = @[
      {"id": "1", "base": "C21575T", "aa": "L5F"}.newStringTable,
      {"id": "2", "base": "G21600T", "aa": "S13I"}.newStringTable]

    let resId = db.createResult()

    check insertResultConcernVariants($resId, @["1", "2"])

    check getResultConcernVariants($resId) == expected

  test "Find Concern Variants in an alignment":
    # Insert a fake concern variant
    db.exec(sql"""INSERT INTO concern_variant (base, aa)
      VALUES (?, ?)""", "A5T", "N2R")

    let resId = db.createResult()
    let seqCVs = getAlignConcernVariants(@["AAAAAAAAAA", "TTTTTTTTTT"])

    check insertResultConcernVariants($resId, seqCVs)

    let expected = @[
      {"id": "3", "base": "A5T", "aa": "N2R"}.newStringTable]
    check getResultConcernVariants($resId) == expected

  test "Save and empty sequence of Variants":
    let resId = db.createResult()
    let seqCVs: seq[string] = @[]

    check insertResultConcernVariants($resId, seqCVs)

suite "Superspreader events":
  setup:
    let dbPath: string = mkdtemp(
      currentSourcePath().parentDir() / "dbtest_") / "test_db.sqlite3"
    putEnv("db", dbPath)
    getConn
    db.migrate()

    db.loadFixtureFor("superspreader_event")
    db.loadFixtureFor("country")
    db.loadFixtureFor("haplogroup")

    putEnv("alignDir", "test_align")

    let expected_evt = {
      "event_sequences": "35", "total_sequences": "3246",
      "last_date": "2021-04-28", "image": "img/superspreads/171.png",
      "n_sequences": "44", "id": "171", "start_date": "2021-02-23",
      "first_date": "2021-02-08", "event_total_sequences": "732",
      "end_date": "2021-02-26", "near_sequences": "93"}.newStringTable

    let expected_seq = @[
      {"event_sequences": "31", "total_sequences": "1367",
       "haplogroup.name": "A2a2c", "last_date": "2020-09-01",
       "image": "img/superspreads/23.png", "n_sequences": "49", "id": "23",
       "country.name": "Canada", "start_date": "2020-08-09",
       "first_date": "2020-07-29", "event_total_sequences": "245",
       "end_date": "2020-08-14", "near_sequences": "65"}.newStringTable,
      {"event_sequences": "59", "total_sequences": "5071",
       "haplogroup.name": "A2a1", "last_date": "2020-11-15",
       "image": "img/superspreads/24.png", "n_sequences": "236", "id": "24",
       "country.name": "Canada", "start_date": "2020-08-09",
       "first_date": "2020-08-05", "event_total_sequences": "326",
       "end_date": "2020-08-17", "near_sequences": "273"}.newStringTable]

  teardown:
    removeDir(getCurrentDir() / "public" / os.getEnv("alignDir"))
    removeDir(dbPath.splitPath().head)

  test "Get all the superspreader events from the DB":
    let allEvents = getAllSuperspreadEvents()
    check len(allEvents) == 26
    check allEvents[^1] == expected_evt

  test "Get a Superspreader event from a list of variants":
    let variantsFound = @["C241T", "C884T", "C3037T", "C14408T", "A23403G"]
    let eventIds = getEvents(superspreadersPhy, variantsFound)
    check eventIds == @["171"]

    check db.get("superspreader_event", eventIds[0]) == expected_evt

  test "Get the country of a Superspreader event":

    check getSuperspreadWithCountries(@["23", "24"]) == expected_seq

  test "Get the Superspreader variants from an alignment":
    let superSequence = load(currentSourcePath().parentDir() / "test_files" /
                             "superspreader.fas")[0]
    let alignmentQuery = preAlign(superSequence.record.chain)
    let alignment = align(alignmentQuery)

    let events = getSuperspreaderEvents(alignment)
    check events == @["171"]

  test "Insert Superspreader events related to a given Result":
    let resultUuid: UUID = db.createResult()
    let eventsIds: seq[string] = @["23", "24"]

    check insertResultSuperspreads($resultUuid, eventsIds)

  test "Get Superspreader events from a given Result at DB level":
    let resultUuid = db.createResult()

    check insertResultSuperspreads($resultUuid, @["23", "24"])

    check getResultSuperspreaderEvents($resultUuid) == expected_seq

  test "Integration test: alignment stores the Superspread event":
    let seqs = collect(newSeqOfCap(1)):
      for s in sequences(currentSourcePath().parentDir() / "test_files" /
                         "superspreader.fas"):
        s.record.chain
    let resultUuid = alignMafft(seqs[0])

    expected_evt["country.name"] = "Turkey"
    expected_evt["haplogroup.name"] = "A2a"

    check getResultSuperspreaderEvents(resultUuid) == @[expected_evt]

  test "Get all the superspreader events from the DB, with countries":
    let allEvents = getAllSuperspreadEvents(countries=true)
    check len(allEvents) == 26

    let event171 = collect(newSeq):
      for ev in allEvents:
        if ev["image"] == "img/superspreads/171.png":
          ev

    var expected_country = expected_evt
    expected_country["country.name"] = "Turkey"
    expected_country["haplogroup.name"] = "A2a"

    check event171[0] == expected_country
