Install Nim
===========

You'll need a C compiler (gcc). The recommended way to install Nim is
https://github.com/dom96/choosenim. On Linux:

  $ curl https://nim-lang.org/choosenim/init.sh -sSf | sh

Follow the post-install instructions, and add the nim binary to your PATH.

Install bio
===========

`bio` is an unpublished package. Request a copy writing an email to
xbello@gmail.com. You'll receive the code and further instructions.

Build the package
=================

Build the package with Nimble (included with Nim). The tool takes care of
dependencies:

  $ nimble build covidphy
  $ nimble jvs

Add a .env file
===============

The `covidphy` binary looks for a file `.env` in the same dir. Create this file
readable only by owner to configure your http server:

  $ touch .env
  $ chmod 600 .env

Edit the file to include the following adjustable settings:

  debug=false
  bindAddr=0.0.0.0
  port=8080
  appName=covidphy.eu
  staticDir=/static
  alignDir=/aligns
  logLevel=lvlDebug
  mailFrom="send.from@server.com"
  mailTo="admin@server.com"
  mail=YourMailPassword
  secretKey=A_Long_Random_Chain_Of_Chars
  db=db.sqlite3
  entrezKey=The_Entrez_Key

Setup the DB
============

The DB should be created empty by running Jester the first time. But tables are
not (by now). Build the programs with Nimble:

  $ nimble build migrate
  $ ./migrate all

Load the needed fixtures from `src/fixtures/name.sql`:

  $ cat src/fixtures/name.sql |sqlite3 db.sqlite3  # One fixture

  $ for F in src/fixtures/*sql; do cat $F |sqlite3 db.sqlite3; done  # *All at once

Running the server
==================

The server is ready to run. Launch it with:

  $ ./covidphy

It won't say anything. You can test it worked by _curling_ localhost at the
port configured in the .env file above. Open another terminal and type:

  $ curl 127.0.0.1:8080
  <!DOCTYPE html>
  ...
  </html>


Configure nginx to listen
=========================

It's a bad practice to expose servers directly to the internet. Use a proxy
like Nginx to transfer the requests to the application. This goes well beyond
the aim of this document, but it can be resumed as:

- Create a `server` directive that points `/` to a `proxy_pass`:

  server {
    location / {
      proxy_pass http://covidphy/;
    }
  }

  Read some docs to set the parameters `proxy_set_header`, `proxy_redirect` and
  others needed.

- Create an `upstream` pointing to the server compiled above. Depending on .env:

  upstream covidphy {
    server 127.0.0.1:8080;
  }

- Fix the `static` dir to be server directly by Nginx instead of the server:

  location /static/ {
    alias /path/of/your/static/;
  }

This is not complicated, but it's also not for beginners. Look for help. It can
be done in minutes if you know what your are doing, but takes days if you don't.

Some other gotchas to nginx configuration:

- Run nginx as proper user.
- Ensure /var/lib/nginx permissions are OK:

  # chown -R userabove:nginx /var/lib/nginx

Configure supervisor to do its thing
====================================

Use supervisor to launch the command. The config .ini file include at least the
following entries: directory, command, user and stdout_logfile.

  # supervisorctl reread
  covidphy_nim.eu: available
  # supervisorctl avail
  covidphy_nim.eu                  avail     auto      999:999
  # supervisorctl update

Manteinance
===========

From the deployment dir:

  $ git pull
  $ nim c -d:danger -o:covidphy src/covidphy
  $ nimble jvs

  ### If needed ###
  $ nim c -o:migrate src/migrate
  $ ./migrate XXXX
  $ cat src/fixtures/name.sql |sqlite3 db.sqlite3
  
  ### The variant frequency data can only be loaded with a script:
  $ src/helpers/updateVariantServer src/fixtures/variant.sql

  ### Restart ###
  $ sudo supervisorctl restart covidphy_nim.eu
