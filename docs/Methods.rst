Data sources
============

All data comes from Gisaid

Data processing
===============

The sequences downloaded from Gisaid are added to our alignment and manually
adjusted using Seaview [#]_. This alignment is feed to the core of the stack,
which extracts the differences between any sequence and the reference
(MN908947.3) and classifies the sequence following a pre-generated phylogeny.

.. [#] https://academic.oup.com/bioinformatics/article/12/6/543/231577

  doi: https://doi.org/10.1093/bioinformatics/12.6.543

Software stack
==============

Nim language was used in the every level of the stack. Nim is one of the most
performant languages [#]_, [#]_, [#]_, usually on par with C, without
sacrificing readability and expressivity. It compiles to native code, thus
avoiding all the pitfalls of interpreted languages like Python, Perl or R when
distributing code, and generates small binaries.

The alignment is performed with a modified MAFFT [#]_, that can be interfaced
directly from Nim through FFI.

It can also compiles to Javascript, targeting the browser and allowing the
developer to write the full stack of a web service in one single language.

The database of choice is SQLite [#]_, as we weighted that the strengths of that
database (easy install, easy management, capable of handling web traffic over
500K hits / day) overtake the weaknesses (not ready for high concurrency
writes, no client/server structure).

The graphics for the webpage are created using Plotly [#]_, which can be
interfaced using Nim both in the frontend and in the backend.

The stack is a monolithic application, but with the *core* (the aligner and
the classifier) decoupled so they can be reused to allow the user to run the
sequences either through a web interface, a GUI or a CLI. We provide in the
repo three main programs:

1. covidphy, the web server that you can reach at www.covidphy.eu.

2. covidphy_cli, a command line interface that we use to classify sequences in
   our pipeline.

3. covidphy_gui, a quite simple graphic interface to people afraid of the CLI
   that prefer to select the input with buttons.

.. [#] https://github.com/kostya/benchmarks

.. [#] https://github.com/def-/nim-benchmarksgame

.. [#] https://www.techempower.com/benchmarks/#section=data-r19&hw=ph&test=json

.. [#] Katoh, Standley 2013 (Molecular Biology and Evolution 30:772-780)
  MAFFT multiple sequence alignment software version 7: improvements in
  performance and usability.

  doi: https://doi.org/10.1093/molbev/mst010

.. [#] SQLite: Hipp, R, et. al. (2020). SQLite (Version 3.33.0) [Computer software].
  SQLite Development Team.  Retrieved October 13, 2020.
  Available from https://www.sqlite.org/download.html

.. [#] https://plotly.com/javascript/

